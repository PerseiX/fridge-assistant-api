<?php

namespace App\ArgumentResolver;

use App\Entity\User;
use App\Model\QueryModel;
use phpDocumentor\Reflection\DocBlock\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class QueryModelValueResolver
 * @package App\ArgumentResolver
 */
class QueryModelValueResolver implements ArgumentValueResolverInterface
{
	/**
	 * @var TokenStorageInterface
	 */
	private $tokenStorage;

	/**
	 * QueryModelValueResolver constructor.
	 *
	 * @param TokenStorageInterface $tokenStorage
	 */
	public function __construct(TokenStorageInterface $tokenStorage)
	{
		$this->tokenStorage = $tokenStorage;
	}

	/**
	 * @param Request          $request
	 * @param ArgumentMetadata $argument
	 *
	 * @return bool
	 */
	public function supports(Request $request, ArgumentMetadata $argument)
	{
		return $argument->getType() === QueryModel::class;
	}

	/**
	 * @param Request          $request
	 * @param ArgumentMetadata $argument
	 *
	 * @return \Generator
	 */
	public function resolve(Request $request, ArgumentMetadata $argument)
	{
		/** @var User $user */
		$user     = $this->tokenStorage->getToken()->getUser();
		$phrase   = $request->get('phrase');
		$fridgeId = $request->get('fridgeId');

		yield new QueryModel($user, $phrase, $fridgeId);
	}
}