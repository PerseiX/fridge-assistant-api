<?php

namespace App\ArgumentResolver;

use App\Entity\RequestUser;
use App\Entity\User;
//use App\Model\RequestUser;
use FOS\UserBundle\Util\UserManipulator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * Class UserValueResolver
 * @package App\ArgumentResolver
 */
class RequestUserValueResolver implements ArgumentValueResolverInterface
{

	/**
	 * @param Request          $request
	 * @param ArgumentMetadata $argument
	 *
	 * @return bool
	 */
	public function supports(Request $request, ArgumentMetadata $argument)
	{
		return $argument->getType() === RequestUser::class;
	}

	/**
	 * Returns the possible value(s).
	 *
	 * @param Request          $request
	 * @param ArgumentMetadata $argument
	 *
	 * @return \Generator
	 */
	public function resolve(Request $request, ArgumentMetadata $argument)
	{
		$requestUser = new RequestUser();

		$requestUser
			->setName($request->get('name'))
			->setSurname($request->get('surname'))
			->setPassword($request->get('password'))
			->setUsername($request->get('username'))
			->setEmail($request->get('email'));

		return yield $requestUser;
	}
}