<?php

namespace App\Controller;

use ApiBundle\Controller\AbstractApiController;
use App\Entity\Fridge;
use App\Entity\FridgeInvitation;
use App\Entity\User;
use App\Form\Type\FridgeFormType;
use App\Form\Type\FridgeInvitationFormType;
use App\Manager\InvitationManager;
use App\Model\FridgeCollectionModel;
use App\Model\FridgeInvitationCollectionModel;
use App\Pusher\Handlers\ProductHandler;
use App\Security\Voters\FridgeVoter;
use App\Security\Voters\InvitationVoter;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Swagger\Annotations as SWG;

/**
 * Class FridgeController
 * @package App\Controller
 */
class FridgeController extends AbstractApiController
{

	/**
	 * @param Request $request
	 *
	 * @Operation(
	 *     tags={"Fridge"},
	 *     summary="Create fridge",
	 *     @SWG\Response(
	 *         response="200",
	 *         description="Created fridge data"
	 *     ),
	 *      @SWG\Parameter(
	 *         name="form",
	 *         in="body",
	 *         description="Fridge model",
	 *         @Model(type=FridgeFormType::class)
	 *     )
	 * )
	 *
	 * @throws \Doctrine\ORM\ORMException
	 * @return Response
	 */
	public function createAction(Request $request)
	{
		$fridge = new Fridge();
		$user   = $this->getUser();
		$fridge
			->setOwner($user)
			->setActive(true)
			->setCreatedAt(new \DateTime());

		/** @var Form $form */
		$form = $this->createForm(FridgeFormType::class, $fridge, []);

		return $this->formResponse($request, $form);
	}

	/**
	 * @param Request $request
	 *
	 * @Operation(
	 *     tags={"Fridge"},
	 *     summary="Join member",
	 *     @SWG\Response(
	 *         response="200",
	 *         description="Join member response"
	 *     ),
	 *      @SWG\Parameter(
	 *         name="form_invitation_type",
	 *         in="body",
	 *         description="Fridge invitation model",
	 *         @Model(type=FridgeInvitationFormType::class)
	 *     )
	 * )
	 *
	 * @return Response
	 * @throws \Doctrine\ORM\ORMException
	 */
	public function joinMemberAction(Request $request)
	{
		$fridgeInvitation = new FridgeInvitation();
		$fridgeInvitation->setInvitedAt(new \DateTime());
		/** @var Form $form */
		$form = $this->createForm(FridgeInvitationFormType::class, $fridgeInvitation, []);

		return $this->formResponse($request, $form);
	}

	/**
	 * @return Response
	 *
	 * @Operation(
	 *     tags={"Fridge"},
	 *     summary="Get fridges",
	 *     @SWG\Response(
	 *         response="200",
	 *         description="Get user fridges"
	 *     )
	 * )
	 *
	 */
	public function getFridgesAction()
	{
		$user    = $this->getUser();
		$fridges = $this->getDoctrine()->getRepository('App:Fridge')->getUserFridges($user);

		return $this->representationResponse($this->transform(new FridgeCollectionModel($fridges)));
	}

	/**
	 * @Operation(
	 *     tags={"Fridge"},
	 *     summary="Get user invitations",
	 *     @SWG\Response(
	 *         response="200",
	 *         description="Get user invitations"
	 *     )
	 * )
	 *
	 */
	public function getInvitationsAction()
	{
		/** @var User $user */
		$user = $this->getUser();

		return $this->representationResponse($this->transform(new FridgeInvitationCollectionModel($user->getInvitations()->getValues())));
	}

	/**
	 * @Operation(
	 *     tags={"Fridge"},
	 *     summary="Get fridge where member",
	 *     @SWG\Response(
	 *         response="200",
	 *         description="Get fridge where member"
	 *     )
	 * )
	 *
	 */
	public function getFridgeWhereMemberAction()
	{
		/** @var User $user */
		$user    = $this->getUser();
		$fridges = $user->getFridges()->getValues();

		return $this->representationResponse($this->transform(new FridgeInvitationCollectionModel($fridges)));
	}

	/**
	 * @param FridgeInvitation $invitation
	 *
	 * @Operation(
	 *     tags={"Fridge"},
	 *     summary="Accept invitation",
	 *     @SWG\Response(
	 *         response="200",
	 *         description="Accept invitation"
	 *     )
	 * )
	 *
	 * @ParamConverter("invitation", class="App\Entity\FridgeInvitation", options={"mapping": {"invitationId": "id"}})
	 *
	 * @return Response
	 * @throws \Doctrine\ORM\ORMException
	 * @throws \Doctrine\ORM\OptimisticLockException
	 */
	public function acceptInvitationAction(FridgeInvitation $invitation)
	{
		$this->denyAccessUnlessGranted(InvitationVoter::ACCEPT_INVITATION, $invitation);
		$fridge = $this->get(InvitationManager::class)->applyInvitation($invitation);

		return $this->representationResponse($this->transform($fridge));
	}

	/**
	 * @param FridgeInvitation $invitation
	 *
	 * @Operation(
	 *     tags={"Fridge"},
	 *     summary="Accept invitation",
	 *     @SWG\Response(
	 *         response="200",
	 *         description="Accept invitation"
	 *     )
	 * )
	 *
	 * @ParamConverter("invitation", class="App\Entity\FridgeInvitation", options={"mapping": {"invitationId": "id"}})
	 *
	 * @return Response
	 * @throws \Doctrine\ORM\ORMException
	 * @throws \Doctrine\ORM\OptimisticLockException
	 */
	public function rejectInvitationAction(FridgeInvitation $invitation)
	{
		$this->denyAccessUnlessGranted(InvitationVoter::REJECT_INVITATION, $invitation);
		$fridge = $this->get(InvitationManager::class)->rejectInvitation($invitation);

		return $this->representationResponse($this->transform($fridge));
	}

	/**
	 * @param Fridge $fridge
	 *
	 * @Operation(
	 *     tags={"Fridge"},
	 *     summary="Remove Fridge",
	 *     @SWG\Response(
	 *         response="200",
	 *         description="Remove fridge"
	 *     )
	 * )
	 * @ParamConverter("fridge", class="App\Entity\Fridge", options={"mapping": {"fridgeId": "id"}})
	 *
	 * @return Response
	 */
	public function removeFridgeAction(Fridge $fridge)
	{
		$this->denyAccessUnlessGranted(FridgeVoter::DELETE, $fridge);
		$em = $this->getDoctrine()->getManager();
		$em->remove($fridge);
		$em->flush();

		$representation = $this->transform($fridge);

		return $this->representationResponse($representation);
	}

	/**
	 * @param Fridge $fridge
	 *
	 * @Operation(
	 *     tags={"Fridge"},
	 *     summary="Leave Fridge",
	 *     @SWG\Response(
	 *         response="200",
	 *         description="Leave fridge"
	 *     )
	 * )
	 *
	 * @ParamConverter("fridge", class="App\Entity\Fridge", options={"mapping": {"fridgeId": "id"}})
	 * @return Response
	 */
	public function leaveFridgeAction(Fridge $fridge)
	{
		$this->denyAccessUnlessGranted(FridgeVoter::LEAVE_FRIDGE, $fridge);

		$em   = $this->getDoctrine()->getManager();
		$user = $this->getUser();
		$fridge->removeMember($user);
		$em->flush();

		return $this->representationResponse($this->transform($fridge));
	}

	/**
	 * @param Fridge $fridge
	 * @param User   $user
	 *
	 * @Operation(
	 *     tags={"Fridge"},
	 *     summary="Leave Fridge",
	 *     @SWG\Response(
	 *         response="200",
	 *         description="Leave fridge"
	 *     )
	 * )
	 * @ParamConverter("fridge", class="App\Entity\Fridge", options={"mapping": {"fridgeId": "id"}})
	 * @ParamConverter("user", class="App\Entity\User", options={"mapping": {"userId": "id"}})
	 *
	 * @return Response
	 */
	public function removeMemberFromFridgeAction(Fridge $fridge, User $user)
	{
//		$this->denyAccessUnlessGranted(FridgeVoter::REMOVE_MEMBER, [$fridge, $user]);

		$em = $this->getDoctrine()->getManager();
		$fridge->removeMember($user);
		$em->flush();

		return $this->representationResponse($this->transform($fridge));
	}
}