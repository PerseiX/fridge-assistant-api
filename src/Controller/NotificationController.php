<?php

namespace App\Controller;

use ApiBundle\Controller\AbstractApiController;
use App\Manager\NotificationManager;
use App\Model\NotificationCollectionModel;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Symfony\Component\HttpFoundation\Response;
use Swagger\Annotations as SWG;

/**
 * Class NotificationController
 * @package App\Controller
 */
class NotificationController extends AbstractApiController
{

	/**
	 * @return Response
	 *
	 * @Operation(
	 *     tags={"Notification"},
	 *     summary="Get notifications",
	 *     @SWG\Response(
	 *         response="200",
	 *         description="Get user notifictions"
	 *     )
	 * )
	 *
	 * @throws \Exception
	 */
	public function getNotificationsAction()
	{
		$notifications = $this->get(NotificationManager::class)->getNotifications($this->getUser());
		$unserialized  = [];
		foreach ($notifications as $notification) {

			$unserialized[] = unserialize($notification);
			//TODO WHEN User change avatar
		}

		$notificationCollection = new NotificationCollectionModel($unserialized);

		return $this->representationResponse($this->transform($notificationCollection));
	}

	/**
	 * @param string $type
	 *
	 * @Operation(
	 *     tags={"Notification"},
	 *     summary="Get notifications",
	 *     @SWG\Response(
	 *         response="200",
	 *         description="Get Grouped notification"
	 *     )
	 * )
	 *
	 * @return Response
	 */
	public function getNotificationByCategory(string $type = null)
	{

		$notifications = $this->get(NotificationManager::class)->getNotifications($this->getUser(), $type);
		$unserialized  = [];
		foreach ($notifications as $notification) {
			$unserialized[] = unserialize($notification);
		}

		$notificationCollection = new NotificationCollectionModel($unserialized);

		return $this->representationResponse($this->transform($notificationCollection));
	}

	/**
	 * @return Response
	 *
	 * @Operation(
	 *     tags={"Notification"},
	 *     summary="Remove notification",
	 *     @SWG\Response(
	 *         response="200",
	 *         description="Get user notifictions"
	 *     )
	 * )
	 *
	 */
	public function removeNotificationAction(string $key)
	{
		$notification = $this->get(NotificationManager::class)->deleteNotification($key);

		return $this->representationResponse($this->transform(unserialize($notification)));
	}

	/**
	 * @return Response
	 *
	 * @Operation(
	 *     tags={"Notification"},
	 *     summary="Has user notification",
	 *     @SWG\Response(
	 *         response="200",
	 *         description="Get true if user has notification"
	 *     )
	 * )
	 *
	 */
	public function hasNotificationAction()
	{
		$hasNotification = $this->get(NotificationManager::class)->hasNotification($this->getUser());

		return $this->handleView($this->view($hasNotification, 200));
	}
}