<?php

namespace App\Controller;

use ApiBundle\Controller\AbstractApiController;
use ApiBundle\Request\PaginatedRequest;
use App\Entity\Fridge;
use App\Entity\FridgeItem;
use App\Entity\Product;
use App\Form\Type\FridgeItemFormType;
use App\Form\Type\ProductFormType;
use App\Manager\FridgeManager;
use App\Manager\NotificationManager;
use App\Model\FridgeItemCollectionModel;
use App\Model\Notification;
use App\Model\NotificationContext;
use App\Model\ProductCollectionModel;
use App\Pusher\Handlers\ProductHandler;
use App\Representation\FridgeItemRepresentation;
use App\Representation\FridgeRepresentation;
use Elastica\Query\BoolQuery;
use Elastica\Query\MatchPhrasePrefix;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Swagger\Annotations as SWG;

/**
 * Class ProductController
 * @package App\Controller
 */
class ProductController extends AbstractApiController
{

	/**
	 * @param Request $request
	 *
	 * @Operation(
	 *     tags={"Product"},
	 *     summary="Create product",
	 *     @SWG\Response(
	 *         response="200",
	 *         description="Created product data"
	 *     ),
	 *      @SWG\Parameter(
	 *         name="form",
	 *         in="body",
	 *         description="Product model",
	 *         @Model(type=ProductFormType::class)
	 *     )
	 * )
	 *
	 * @throws \Doctrine\ORM\ORMException
	 * @return Response
	 */
	public function createProductAction(Request $request)
	{
		/** @var Form $form */
		$form = $this->createForm(ProductFormType::class, new Product());

		return $this->formResponse($request, $form);
	}

	/**
	 * @param Fridge  $fridge
	 * @param Product $product
	 *
	 * @Operation(
	 *     tags={"Product"},
	 *     summary="Add product",
	 *     @SWG\Response(
	 *         response="200",
	 *         description="Add product to fridge"
	 *     )
	 * )
	 *
	 * @ParamConverter(name="fridge", class="App\Entity\Fridge", options={"mapping": {"fridgeId": "id"}})
	 * @ParamConverter(name="product", class="App\Entity\Product", options={"mapping": {"productId": "id"}})
	 *
	 * @return Response
	 * @throws \Doctrine\ORM\ORMException
	 * @throws \Doctrine\ORM\OptimisticLockException
	 * @throws \Pusher\PusherException
	 */
	public function addProductAction(Fridge $fridge, Product $product)
	{
		/** @var FridgeItemRepresentation $fridgeItemRepresentation */
		$fridgeItem               = $this->get(FridgeManager::class)->addFridgeItem($product, $fridge, $this->getUser());
		$fridgeItemRepresentation = $this->transform($fridgeItem);
		$this->get(ProductHandler::class)->addProductFromFridge($fridgeItemRepresentation);

		$notificationContext = new NotificationContext($this->getUser(), $fridgeItem, Notification::PRODUCT_CONTEXT);
		$this->get(NotificationManager::class)->createAddNotification($notificationContext);

		return $this->representationResponse($fridgeItemRepresentation);
	}

	/**
	 * @param Fridge           $fridge
	 * @param PaginatedRequest $paginatedRequest
	 *
	 * @Operation(
	 *     tags={"Product"},
	 *     summary="Get products collection",
	 *     @SWG\Response(
	 *         response="200",
	 *         description="Get products collection"
	 *     ),
	 *     @SWG\Parameter(
	 *         name="page",
	 *         in="query",
	 *         description="Page",
	 *         required=false,
	 *         type="number",
	 *         default=1
	 *     ),
	 *     @SWG\Parameter(
	 *         name="limit",
	 *         in="query",
	 *         description="Limit",
	 *         required=false,
	 *         type="number",
	 *         default=10
	 *     )
	 * )
	 *
	 * @ParamConverter(name="fridge", class="App\Entity\Fridge", options={"mapping": {"fridgeSlug": "slug"}})
	 * @return Response
	 */
	public function getProductsAction(Fridge $fridge, PaginatedRequest $paginatedRequest)
	{
		$collection = $this->getDoctrine()->getRepository('App:FridgeItem')->getCollectionQuery($fridge);

		return $this->paginatedResponse(FridgeItemCollectionModel::class, $collection, $paginatedRequest, [
			'fridgeSlug' => $fridge->getSlug()
		]);
	}

	/**
	 * @Operation(
	 *     tags={"Product"},
	 *     summary="Get product suggestion",
	 *     @SWG\Response(
	 *         response="200",
	 *         description="Get product suggestion"
	 *     )
	 * )
	 *
	 * @return Response
	 */
	public function productSuggestAction(Request $request)
	{
		$phrase       = $request->get('phrase');
		$elastica     = $this->get('fos_elastica.finder.product_index.product');
		$booleanQuery = new BoolQuery();

		$pregMatch = new MatchPhrasePrefix();
		$pregMatch->setField('name', $phrase);
		$booleanQuery->addMust($pregMatch);
		$data = $elastica->find($booleanQuery);

		return $this->representationResponse($this->transform(new ProductCollectionModel($data)));
	}

	/**
	 * @param FridgeItem $fridgeItem
	 *
	 * @Operation(
	 *     tags={"Product"},
	 *     summary="Add product",
	 *     @SWG\Response(
	 *         response="200",
	 *         description="Add product to fridge"
	 *     )
	 * )
	 *
	 * @ParamConverter(name="fridgeItem", class="App\Entity\FridgeItem", options={"mapping": {"productItemId": "id"}})
	 *
	 * @return Response
	 * @throws \Pusher\PusherException
	 */
	public function removeProductFromFridgeAction(FridgeItem $fridgeItem)
	{
		/** @var FridgeItemRepresentation $fridgeItemRepresentation */
		$fridgeItemRepresentation = $this->transform($fridgeItem);
		$this->get(ProductHandler::class)->removeProductFromFridge($fridgeItemRepresentation);

		$notificationContext = new NotificationContext($this->getUser(), $fridgeItem, Notification::PRODUCT_CONTEXT);
		$this->get(NotificationManager::class)->createDeleteNotification($notificationContext);

		$fridge = $fridgeItem->getFridge();
		$fridge->removeFridgeItem($fridgeItem);

		$em = $this->getDoctrine()->getManager();
		$em->flush();

		return $this->representationResponse($fridgeItemRepresentation);
	}

	/**
	 * @param Request $request
	 *
	 * @Operation(
	 *     tags={"Product"},
	 *     summary="Get product",
	 *     @SWG\Response(
	 *         response="200",
	 *         description="Get product by name"
	 *     )
	 * )
	 *
	 *
	 * @return Response
	 */
	public function getProductByName(Request $request)
	{
		$productName = $request->get('productName');
		$product     = $this->getDoctrine()->getRepository('App:Product')->findOneBy(['name' => $productName]);

		if (null === $product) {
			return $this->response(200);
		}

		return $this->representationResponse($this->transform($product));
	}

	/**
	 * @param FridgeItem $fridgeItem
	 *
	 * @param Request    $request
	 *
	 * @Operation(
	 *     tags={"Product"},
	 *     summary="Edit product item",
	 *     @SWG\Response(
	 *         response="200",
	 *         description="Edit product item"
	 *     ),
	 *      @SWG\Parameter(
	 *         name="form",
	 *         in="body",
	 *         description="Product item model",
	 *         @Model(type=FridgeItemFormType::class)
	 *     )
	 * )
	 * @ParamConverter(name="fridgeItem", class="App\Entity\FridgeItem", options={"mapping": {"fridgeItemId": "id"}})
	 *
	 * @return Response
	 * @throws \Doctrine\ORM\ORMException
	 */
	public function editFridgeItemAction(FridgeItem $fridgeItem, Request $request)
	{
		/** @var Form $form */
		$form = $this->createForm(FridgeItemFormType::class, $fridgeItem);

		$notificationContext = new NotificationContext($this->getUser(), $fridgeItem, Notification::PRODUCT_CONTEXT);
		$this->get(NotificationManager::class)->createEditNotification($notificationContext);

		return $this->formResponse($request, $form);
	}
}