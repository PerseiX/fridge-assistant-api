<?php

namespace App\Controller;

use ApiBundle\Request\PaginatedRequest;
use App\Entity\ShoppingListItem;
use App\Form\Type\ShoppingListItemType;
use App\Manager\NotificationManager;
use App\Manager\ShoppingListManager;
use App\Model\Notification;
use App\Model\NotificationContext;
use App\Model\ShoppingListItemCollectionModel;
use App\Pusher\Handlers\ShoppingListHandler;
use App\Representation\ShoppingListItemRepresentation;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use ApiBundle\Controller\AbstractApiController;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Swagger\Annotations as SWG;
use App\Entity\Product;
use App\Entity\Fridge;

/**
 * Class ShoppingListController
 * @package App\Controller
 */
class ShoppingListController extends AbstractApiController
{
	/**
	 * @param Fridge  $fridge
	 * @param Product $product
	 *
	 * @Operation(
	 *     tags={"Shopping list"},
	 *     summary="Create shopping list",
	 *     @SWG\Response(
	 *         response="200",
	 *         description="Get product suggestion"
	 *     )
	 * )
	 * @ParamConverter(name="fridge", class="App\Entity\Fridge", options={"mapping": {"fridgeId": "id"}})
	 * @ParamConverter(name="product", class="App\Entity\Product", options={"mapping": {"productId": "id"}})
	 *
	 * @return Response
	 * @throws \Doctrine\ORM\ORMException
	 * @throws \Doctrine\ORM\OptimisticLockException
	 */
	public function addShoppingListItemAction(Fridge $fridge, Product $product)
	{
		/** @var ShoppingListItemRepresentation $representation */
		$shoppingListItem = $this->get(ShoppingListManager::class)->addShoppingListItem($product, $fridge, $this->getUser());
		$representation   = $this->transform($shoppingListItem);
		$this->get(ShoppingListHandler::class)->addShoppingListItem($representation);

		$notificationContext = new NotificationContext($this->getUser(), $shoppingListItem, Notification::SHOPPING_LIST_CONTEXT);
		$this->get(NotificationManager::class)->createAddNotification($notificationContext);

		return $this->representationResponse($representation);
	}

	/**
	 * @param Fridge           $fridge
	 * @param PaginatedRequest $paginatedRequest
	 *
	 * @Operation(
	 *     tags={"Shopping list"},
	 *     summary="Get shipping list",
	 *     @SWG\Response(
	 *         response="200",
	 *         description="Get shipping ist"
	 *     ),
	 *     @SWG\Parameter(
	 *         name="page",
	 *         in="query",
	 *         description="Page",
	 *         required=false,
	 *         type="number",
	 *         default=1
	 *     ),
	 *     @SWG\Parameter(
	 *         name="limit",
	 *         in="query",
	 *         description="Limit",
	 *         required=false,
	 *         type="number",
	 *         default=10
	 *     )
	 * )
	 * @ParamConverter(name="fridge", class="App\Entity\Fridge", options={"mapping": {"fridgeSlug": "slug"}})
	 * @return Response
	 */
	public function getShippingList(Fridge $fridge, PaginatedRequest $paginatedRequest)
	{
		$collection = $this->getDoctrine()->getRepository('App:ShoppingListItem')->getCollectionQuery($fridge);

		return $this->paginatedResponse(ShoppingListItemCollectionModel::class, $collection, $paginatedRequest, [
			'fridgeSlug' => $fridge->getSlug()
		]);
	}

	/**
	 * @param Fridge           $fridge
	 * @param ShoppingListItem $shoppingListItem
	 *
	 * @Operation(
	 *     tags={"Shopping list"},
	 *     summary="Remove product from shopping list",
	 *     @SWG\Response(
	 *         response="200",
	 *         description="Remove product from shopping list"
	 *     )
	 * )
	 * @ParamConverter(name="fridge", class="App\Entity\Fridge", options={"mapping": {"fridgeId": "id"}})
	 * @ParamConverter(name="shoppingListItem", class="App\Entity\ShoppingListItem", options={"mapping": {"shoppingListId": "id"}})
	 *
	 * @return Response
	 * @throws \Exception
	 */
	public function removeShoppingListAction(Fridge $fridge, ShoppingListItem $shoppingListItem)
	{
		/** @var ShoppingListItemRepresentation $representation */
		$representation = $this->transform($shoppingListItem);
		$this->get(ShoppingListHandler::class)->removeShoppingListItem($representation);

		$notificationContext = new NotificationContext($this->getUser(), $shoppingListItem, Notification::SHOPPING_LIST_CONTEXT);
		$this->get(NotificationManager::class)->createDeleteNotification($notificationContext);

		$fridge->removeShoppingListItem($shoppingListItem);
		$this->getDoctrine()->getManager()->flush();

		return $this->representationResponse($representation);
	}

	/**
	 * @param ShoppingListItem $shoppingListItem
	 * @param Request          $request
	 *
	 * @Operation(
	 *     tags={"Shopping list"},
	 *     summary="Edit shoppinh list itme",
	 *     @SWG\Response(
	 *         response="200",
	 *         description="Edit shopping list item"
	 *     ),
	 *      @SWG\Parameter(
	 *         name="form",
	 *         in="body",
	 *         description="Shopping list item model",
	 *         @Model(type=ShoppingListItemType::class)
	 *     )
	 * )
	 * @ParamConverter(name="shoppingListItem", class="App\Entity\ShoppingListItem", options={"mapping": {"shoppingListId": "id"}})
	 *
	 * @return Response
	 * @throws \Doctrine\ORM\ORMException
	 */
	public function editShoppingListItemAction(ShoppingListItem $shoppingListItem, Request $request)
	{
		/** @var Form $form */
		$form = $this->createForm(ShoppingListItemType::class, $shoppingListItem);

		$notificationContext = new NotificationContext($this->getUser(), $shoppingListItem, Notification::SHOPPING_LIST_CONTEXT);
		$this->get(NotificationManager::class)->createEditNotification($notificationContext);

		return $this->formResponse($request, $form);
	}
}