<?php

namespace App\Controller;

use ApiBundle\Controller\AbstractApiController;
use App\Entity\RequestUser;
use App\Entity\User;
use App\Form\Type\UserFormType;
use App\Manager\UserManager;
use App\Model\QueryModel;
use App\Model\UserCollectionModel;
use App\Search\SuggesterQuery;
use App\Services\UserDetectLoginStatus;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Swagger\Annotations as SWG;

/**
 * Class DefaultController
 * @package App\Controller
 */
class UserController extends AbstractApiController
{

	/**
	 * @param RequestUser $requestUser
	 *
	 * @return Response
	 */
	public function registryAction(RequestUser $requestUser): Response
	{

		$userCreateResponse = $this->get(UserManager::class)->registry($requestUser);
		if (true === $userCreateResponse instanceof User) {
			$userCreateResponse->setEnabled(true);

			return $this->representationResponse($this->transform($userCreateResponse));
		}

		return new JsonResponse($userCreateResponse, Response::HTTP_CONFLICT);
	}

	/**
	 * @param Request $request
	 *
	 * @Operation(
	 *     tags={"User"},
	 *     summary="Check username exist",
	 *     @SWG\Response(
	 *         response="200",
	 *         description="Check if username already exist"
	 *     )
	 * )
	 *
	 * @return JsonResponse
	 * @throws \Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException
	 */
	public function usernameExistAction(Request $request)
	{
		$username = $request->get('username');
		$user     = $this->getDoctrine()->getRepository('App:User')->findOneBy(['username' => $username]);

		if ($this->get(UserDetectLoginStatus::class)->theSameUsername() === true) {
			return new JsonResponse(false);
		}

		if (null === $user) {
			return new JsonResponse(false);
		}

		return new JsonResponse(true);
	}

	/**
	 * @param Request $request
	 *
	 * @Operation(
	 *     tags={"User"},
	 *     summary="Check email exist",
	 *     @SWG\Response(
	 *         response="200",
	 *         description="Check if email already exist"
	 *     )
	 * )
	 *
	 * @return JsonResponse
	 * @throws \Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException
	 */
	public function emailExistAction(Request $request)
	{
		$email = $request->get('email');
		$user  = $this->getDoctrine()->getRepository('App:User')->findOneBy(['email' => $email]);

		if ($this->get(UserDetectLoginStatus::class)->theSameEmail() === true) {
			return new JsonResponse(false);
		}

		if (null === $user) {
			return new JsonResponse(false);
		}

		return new JsonResponse(true);
	}

	/**
	 * @Operation(
	 *     tags={"User"},
	 *     summary="Get user details",
	 *     @SWG\Response(
	 *         response="200",
	 *         description="Return user details when user authorized."
	 *     )
	 * )
	 *
	 * @return Response
	 */
	public function userDetailsAction(): Response
	{
		return $this->representationResponse($this->transform($this->getUser()));
	}

	/**
	 * @param Request $request
	 *
	 * @Operation(
	 *     tags={"User"},
	 *     summary="Update user details",
	 *     @SWG\Response(
	 *         response="200",
	 *         description="Returned when successful"
	 *     ),
	 *      @SWG\Parameter(
	 *         name="form",
	 *         in="body",
	 *         description="User model",
	 *         @Model(type=UserFormType::class)
	 *     )
	 * )
	 *
	 * @return Response
	 * @throws \Doctrine\ORM\ORMException
	 */
	public function userUpdateAction(Request $request): Response
	{
		/** @var Form $form */
		$form = $this->createForm(UserFormType::class, $this->getUser(), [
			'validation_groups' => 'update'
		]);

		return $this->formResponse($request, $form);
	}

	/**
	 * @param QueryModel $queryModel
	 * @Operation(
	 *     tags={"User"},
	 *     summary="Accept invitation",
	 *     @SWG\Response(
	 *         response="200",
	 *         description="Accept invitation"
	 *     )
	 * )
	 *
	 * @return Response
	 */
	public function userSuggesterAction(QueryModel $queryModel): Response
	{
		$suggestedEmails = $this->get(SuggesterQuery::class)->getSuggestedEmails($queryModel);

		return $this->representationResponse($this->transform(new UserCollectionModel($suggestedEmails)));
	}

	/**
	 * @param Request $request
	 * @Operation(
	 *     tags={"User"},
	 *     summary="Change user password",
	 *     @SWG\Response(
	 *         response="200",
	 *         description="Change user password"
	 *     )
	 * )
	 *
	 * @return Response
	 */
	public function changePasswordAction(Request $request)
	{
		/** @var User $user */
		$user        = $this->getUser();
		$password    = $request->get('password');
		$newPassword = $request->get('newPassword');

		try {
			$user = $this->get(UserManager::class)->resetPassword($user, $password, $newPassword);
		} catch (\Exception $e) {
			return $this->response(Response::HTTP_BAD_REQUEST);
		}

		return $this->representationResponse($user);
	}
}