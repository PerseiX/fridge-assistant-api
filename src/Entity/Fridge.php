<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class Fridge
 * @package App\Entity
 *
 * @ORM\Entity(repositoryClass="App\Entity\Repository\FridgeRepository")
 * @ORM\Table()
 */
class Fridge
{
	/**
	 * @var int
	 *
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", length=128)
	 */
	protected $name;

	/**
	 * @var bool
	 *
	 * @ORM\Column(type="boolean")
	 */
	protected $active;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", length=128, nullable=true)
	 * @Gedmo\Slug(fields={"name"})
	 */
	protected $slug;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	protected $createdAt;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="fridges", cascade={"persist"})
	 * @ORM\JoinColumn(referencedColumnName="id")
	 */
	protected $members;

	/**
	 * @var User
	 *
	 * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="ownedFridges")
	 * @ORM\JoinColumn(referencedColumnName="id")
	 */
	protected $owner;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\OneToMany(targetEntity="App\Entity\FridgeInvitation", mappedBy="invitedFridge", cascade={"REMOVE"})
	 */
	protected $invitations;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\OneToMany(targetEntity="App\Entity\FridgeItem", mappedBy="fridge", orphanRemoval=true)
	 */
	protected $fridgeItems;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\OneToMany(targetEntity="App\Entity\ShoppingListItem", mappedBy="fridge", orphanRemoval=true)
	 */
	protected $shoppingListItems;

	/**
	 * Fridge constructor.
	 */
	public function __construct()
	{
		$this->members           = new ArrayCollection();
		$this->invitations       = new ArrayCollection();
		$this->fridgeItems       = new ArrayCollection();
		$this->shoppingListItems = new ArrayCollection();
	}

	/**
	 * @return int|null
	 */
	public function getId(): ?int
	{
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getName(): ?string
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 *
	 * @return Fridge
	 */
	public function setName(?string $name): Fridge
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isActive(): ?bool
	{
		return $this->active;
	}

	/**
	 * @param bool $active
	 *
	 * @return Fridge
	 */
	public function setActive(?bool $active): Fridge
	{
		$this->active = $active;

		return $this;
	}

	/**
	 * @return null|string
	 */
	public function getSlug(): ?string
	{
		return $this->slug;
	}

	/**
	 * @param null|string $slug
	 *
	 * @return Fridge
	 */
	public function setSlug(?string $slug): Fridge
	{
		$this->slug = $slug;

		return $this;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getMembers()
	{
		return $this->members;
	}

	/**
	 * @return \DateTime
	 */
	public function getCreatedAt(): \DateTime
	{
		return $this->createdAt;
	}

	/**
	 * @param \DateTime $createdAt
	 *
	 * @return Fridge
	 */
	public function setCreatedAt(\DateTime $createdAt): Fridge
	{
		$this->createdAt = $createdAt;

		return $this;
	}

	/**
	 * @param User $user
	 *
	 * @return Fridge
	 */
	public function addMember(?User $user): Fridge
	{
		if (false === $this->members->contains($user)) {
			$this->members->add($user);
			$user->addFridge($this);
		}

		return $this;
	}

	/**
	 * @param User $user
	 *
	 * @return Fridge
	 */
	public function removeMember(User $user): Fridge
	{
		if (true === $this->members->contains($user)) {
			$this->members->removeElement($user);
			$user->removeFridge($this);
		}

		return $this;
	}

	/**
	 * @return User|null
	 */
	public function getOwner(): ?User
	{
		return $this->owner;
	}

	/**
	 * @param User $owner
	 *
	 * @return Fridge
	 */
	public function setOwner(?User $owner): Fridge
	{
		$this->owner = $owner;

		return $this;
	}

	/**
	 * @param FridgeInvitation $fridgeInvitation
	 *
	 * @return ArrayCollection
	 */
	public function addInvitation(FridgeInvitation $fridgeInvitation)
	{
		if (false === $this->invitations->contains($fridgeInvitation)) {
			$this->invitations->add($fridgeInvitation);
			$fridgeInvitation->setInvitedUser($this);
		}

		return $this->invitations;
	}

	/**
	 * @param FridgeInvitation $fridgeInvitation
	 *
	 * @return ArrayCollection
	 */
	public function removeInvitation(FridgeInvitation $fridgeInvitation)
	{
		if (true === $this->invitations->contains($fridgeInvitation)) {
			$this->invitations->removeElement($fridgeInvitation);
			$fridgeInvitation->setInvitedUser(null);
		}

		return $this->invitations;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getInvitations()
	{
		return $this->invitations;
	}

	/**
	 * @param ShoppingListItem $shoppingListItem
	 *
	 * @return Fridge|null
	 */
	public function addShoppingListItem(ShoppingListItem $shoppingListItem): ?Fridge
	{
		if (false === $this->shoppingListItems->contains($shoppingListItem)) {
			$this->shoppingListItems->add($shoppingListItem);
			$shoppingListItem->setFridge($this);
		}

		return $this;
	}

	/**
	 * @param ShoppingListItem $shoppingListItem
	 *
	 * @return Fridge|null
	 */
	public function removeShoppingListItem(ShoppingListItem $shoppingListItem): ?Fridge
	{
		if (true === $this->shoppingListItems->contains($shoppingListItem)) {
			$this->shoppingListItems->removeElement($shoppingListItem);
			$shoppingListItem->setFridge(null);
		}

		return $this;
	}

	/**
	 * @param FridgeItem $fridgeItem
	 *
	 * @return Fridge|null
	 */
	public function addFridgeItem(FridgeItem $fridgeItem): ?Fridge
	{
		if (false === $this->fridgeItems->contains($fridgeItem)) {
			$this->fridgeItems->add($fridgeItem);
			$fridgeItem->setFridge($this);
		}

		return $this;
	}

	/**
	 * @param FridgeItem $fridgeItem
	 *
	 * @return Fridge|null
	 */
	public function removeFridgeItem(FridgeItem $fridgeItem): ?Fridge
	{
		if (true === $this->fridgeItems->contains($fridgeItem)) {
			$this->fridgeItems->removeElement($fridgeItem);
			$fridgeItem->setFridge(null);
		}

		return $this;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getFridgeItems()
	{
		return $this->fridgeItems;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getShoppingListItems()
	{
		return $this->shoppingListItems;
	}
}