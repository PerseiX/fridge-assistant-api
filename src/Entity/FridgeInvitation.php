<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class FridgeInvitation
 *
 * @ORM\Entity()
 * @ORM\Table()
 *
 * @package App\Entity
 */
class FridgeInvitation
{
	/**
	 * @var int
	 *
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var User|null
	 *
	 * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="invitations")
	 * @ORM\JoinColumn(referencedColumnName="id")
	 */
	protected $invitedUser;

	/**
	 * @var Fridge|null
	 *
	 * @ORM\ManyToOne(targetEntity="App\Entity\Fridge", inversedBy="invitations")
	 * @ORM\JoinColumn(referencedColumnName="id")
	 */
	protected $invitedFridge;

	/**
	 * @var boolean|null
	 *
	 * @ORM\Column(type="boolean")
	 */
	protected $accepted;

	/**
	 * @var \DateTime|null
	 *
	 * @ORM\Column(type="datetime")
	 */
	protected $invitedAt;

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return User|null
	 */
	public function getInvitedUser(): ?User
	{
		return $this->invitedUser;
	}

	/**
	 * @param User|null $invitedUser
	 *
	 * @return FridgeInvitation
	 */
	public function setInvitedUser(?User $invitedUser): FridgeInvitation
	{
		$this->invitedUser = $invitedUser;

		return $this;
	}

	/**
	 * @return Fridge|null
	 */
	public function getInvitedFridge(): ?Fridge
	{
		return $this->invitedFridge;
	}

	/**
	 * @param Fridge|null $invitedFridge
	 *
	 * @return FridgeInvitation
	 */
	public function setInvitedFridge(?Fridge $invitedFridge): FridgeInvitation
	{
		$this->invitedFridge = $invitedFridge;

		return $this;
	}

	/**
	 * @return bool|null
	 */
	public function getAccepted(): ?bool
	{
		return $this->accepted;
	}

	/**
	 * @param bool|null $accepted
	 *
	 * @return FridgeInvitation
	 */
	public function setAccepted(?bool $accepted): FridgeInvitation
	{
		$this->accepted = $accepted;

		return $this;
	}

	/**
	 * @return \DateTime|null
	 */
	public function getInvitedAt(): ?\DateTime
	{
		return $this->invitedAt;
	}

	/**
	 * @param \DateTime|null $invitedAt
	 *
	 * @return FridgeInvitation
	 */
	public function setInvitedAt(?\DateTime $invitedAt): FridgeInvitation
	{
		$this->invitedAt = $invitedAt;

		return $this;
	}

}