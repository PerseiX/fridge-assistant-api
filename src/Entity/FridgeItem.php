<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class FridgeItem
 * @package App\Entity
 *
 * @ORM\Entity(repositoryClass="App\Entity\Repository\FridgeItemRepository")
 * @ORM\Table()
 */
class FridgeItem implements ProductItemInterface
{
	/**
	 * @var int|null
	 *
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var Fridge|null
	 *
	 * @ORM\ManyToOne(targetEntity="App\Entity\Fridge", inversedBy="fridgeItems", cascade={"PERSIST"})
	 * @ORM\JoinColumn(referencedColumnName="id")
	 */
	private $fridge;

	/**
	 * @var Product|null
	 *
	 * @ORM\ManyToOne(targetEntity="App\Entity\Product", inversedBy="fridgeItems", cascade={"PERSIST"})
	 * @ORM\JoinColumn(referencedColumnName="id")
	 */
	protected $product;

	/**
	 * @var User|null
	 *
	 * @ORM\ManyToOne(targetEntity="App\Entity\User",  inversedBy="fridgeItems", cascade={"PERSIST"})
	 * @ORM\JoinColumn(referencedColumnName="id")
	 */
	protected $createdBy;

	/**
	 * @var int|null
	 *
	 * @ORM\Column(type="integer", options={"default": 0})
	 */
	protected $amount;

	/**
	 * @var \DateTime|null
	 *
	 * @ORM\Column(type="datetime")
	 */
	protected $createdAt;

	/**
	 * @var string|null
	 *
	 * @ORM\Column(type="string", length=256, nullable=true)
	 */
	protected $description;

	/**
	 * @return int|null
	 */
	public function getId(): ?int
	{
		return $this->id;
	}

	/**
	 * @return Fridge|null
	 */
	public function getFridge(): ?Fridge
	{
		return $this->fridge;
	}

	/**
	 * @param Fridge|null $fridge
	 *
	 * @return FridgeItem
	 */
	public function setFridge(?Fridge $fridge): FridgeItem
	{
		$this->fridge = $fridge;

		return $this;
	}

	/**
	 * @return Product|null
	 */
	public function getProduct(): ?Product
	{
		return $this->product;
	}

	/**
	 * @param Product|null $product
	 *
	 * @return FridgeItem
	 */
	public function setProduct(?Product $product): FridgeItem
	{
		$this->product = $product;

		return $this;
	}

	/**
	 * @return User|null
	 */
	public function getCreatedBy(): ?User
	{
		return $this->createdBy;
	}

	/**
	 * @param User|null $createdBy
	 *
	 * @return FridgeItem
	 */
	public function setCreatedBy(?User $createdBy): FridgeItem
	{
		$this->createdBy = $createdBy;

		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getAmount(): ?int
	{
		return $this->amount;
	}

	/**
	 * @param int|null $amount
	 *
	 * @return FridgeItem
	 */
	public function setAmount(?int $amount): FridgeItem
	{
		$this->amount = $amount;

		return $this;
	}

	/**
	 * @return \DateTime|null
	 */
	public function getCreatedAt(): ?\DateTime
	{
		return $this->createdAt;
	}

	/**
	 * @param \DateTime|null $createdAt
	 *
	 * @return FridgeItem
	 */
	public function setCreatedAt(?\DateTime $createdAt): FridgeItem
	{
		$this->createdAt = $createdAt;

		return $this;
	}

	/**
	 * @return null|string
	 */
	public function getDescription(): ?string
	{
		return $this->description;
	}

	/**
	 * @param null|string $description
	 *
	 * @return FridgeItem
	 */
	public function setDescription(?string $description): FridgeItem
	{
		$this->description = $description;

		return $this;
	}

}