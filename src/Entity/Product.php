<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Product
 * @package App\Entity
 *
 * @ORM\Table()
 * @ORM\Entity()
 */
class Product
{
	// TODO AMOUNT !!!??!?!??!?!?!?!?
	/**
	 * @var int|null
	 *
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string|null
	 *
	 * @ORM\Column(type="string", length=512)
	 */
	private $name;

	/**
	 * @var string|null
	 *
	 * @ORM\Column(type="string", length=512, nullable=true)
	 */
	private $description;

	/**
	 * @var ArrayCollection|null
	 *
	 * @ORM\ManyToMany(targetEntity="App\Entity\Fridge", inversedBy="products", cascade={"PERSIST"})
	 * @ORM\JoinColumn(referencedColumnName="id")
	 */
	private $fridges;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\OneToMany(targetEntity="App\Entity\ShoppingListItem", mappedBy="product")
	 */
	private $shoppingListItems;

	/**
	 * Product constructor.
	 */
	public function __construct()
	{
		$this->fridges           = new ArrayCollection();
		$this->shoppingListItems = new ArrayCollection();
	}

	/**
	 * @return int|null
	 */
	public function getId(): ?int
	{
		return $this->id;
	}

	/**
	 * @return null|string
	 */
	public function getName(): ?string
	{
		return $this->name;
	}

	/**
	 * @param null|string $name
	 *
	 * @return Product
	 */
	public function setName(?string $name): Product
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * @return null|string
	 */
	public function getDescription(): ?string
	{
		return $this->description;
	}

	/**
	 * @param null|string $description
	 *
	 * @return Product
	 */
	public function setDescription(?string $description): Product
	{
		$this->description = $description;

		return $this;
	}

	/**
	 * @return ArrayCollection|null
	 */
	public function getFridges()
	{
		return $this->fridges;
	}

	/**
	 * @param Fridge $fridge
	 *
	 * @return Product
	 */
	public function addFridge(Fridge $fridge): Product
	{
		if (false === $this->fridges->contains($fridge)) {
			$this->fridges->add($fridge);
			$fridge->addProduct($this);
		}

		return $this;
	}

	/**
	 * @param Fridge $fridge
	 *
	 * @return $this
	 */
	public function removeFridge(Fridge $fridge)
	{
		if (true === $this->fridges->contains($fridge)) {
			$this->fridges->removeElement($fridge);
			$fridge->removeProduct($this);
		}

		return $this;
	}

	/**
	 * @param ShoppingListItem $shoppingListItem
	 *
	 * @return Product|null
	 */
	public function addShoppingListItem(ShoppingListItem $shoppingListItem): ?Product
	{
		if (false === $this->shoppingListItems->contains($shoppingListItem)) {
			$this->shoppingListItems->add($shoppingListItem);
			$shoppingListItem->setProduct($this);
		}

		return $this;
	}

	/**
	 * @param ShoppingListItem $shoppingListItem
	 *
	 * @return Product|null
	 */
	public function removeShoppingListItem(ShoppingListItem $shoppingListItem): ?Product
	{
		if (true === $this->shoppingListItems->contains($shoppingListItem)) {
			$this->shoppingListItems->removeElement($shoppingListItem);
			$shoppingListItem->setProduct(null);
		}

		return $this;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getShoppingListItems()
	{
		return $this->shoppingListItems;
	}

}