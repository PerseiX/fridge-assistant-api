<?php

namespace App\Entity;

/**
 * Interface ProductItemInterface
 * @package App\Entity
 */
interface ProductItemInterface
{
	/**
	 * @return Fridge|null
	 */
	public function getFridge(): ?Fridge;

	/**
	 * @return Product|null
	 */
	public function getProduct(): ?Product;

	/**
	 * @return int|null
	 */
	public function getId(): ?int;
}