<?php

namespace App\Entity\Repository;

use App\Entity\Fridge;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;

/**
 * Class FridgeItemRepository
 * @package App\Entity\Repository
 */
class FridgeItemRepository extends EntityRepository
{

	/**
	 * @param Fridge $fridge
	 *
	 * @return \Doctrine\ORM\Query
	 */
	public function getCollectionQuery(Fridge $fridge)
	{
		return $this->createQueryBuilder('fi')
		            ->andWhere('fi.fridge = :fridge')
		            ->setParameter('fridge', $fridge)
		            ->addOrderBy('fi.id', Criteria::DESC)
		            ->getQuery();
	}
}