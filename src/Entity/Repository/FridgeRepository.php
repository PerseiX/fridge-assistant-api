<?php

namespace App\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class FridgeRepository
 * @package App\Entity\Repository
 */
class FridgeRepository extends EntityRepository
{

	public function getUserFridges(UserInterface $user)
	{
		return $this->createQueryBuilder('f')
		            ->andWhere('f.owner = :user')
		            ->setParameter('user', $user)
		            ->getQuery()
		            ->getResult();
	}
}