<?php

namespace App\Entity\Repository;

use App\Entity\Fridge;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;

/**
 * Class ShoppingListItemRepository
 * @package App\Entity\Repository
 */
class ShoppingListItemRepository extends EntityRepository
{

	/**
	 * @param Fridge $fridge
	 *
	 * @return \Doctrine\ORM\Query
	 */
	public function getCollectionQuery(Fridge $fridge)
	{
		return $this->createQueryBuilder('sl')
		            ->andWhere('sl.fridge = :fridge')
		            ->setParameter('fridge', $fridge)
		            ->addOrderBy('sl.id', Criteria::DESC)
		            ->getQuery();
	}
}