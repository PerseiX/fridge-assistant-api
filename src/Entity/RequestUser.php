<?php

namespace App\Entity;

/**
 * Class RequestUser
 * @package App\Model
 */
class RequestUser
{
	/**
	 * @var  string
	 */
	private $username;

	/**
	 * @var string
	 */
	private $email;

	/**
	 * @var string
	 */
	private $password;

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var string
	 */
	private $surname;

	/**
	 * @return null|string
	 */
	public function getUsername(): ?string
	{
		return $this->username;
	}

	/**
	 * @param string|null $username
	 *
	 * @return RequestUser
	 */
	public function setUsername(string $username = null): RequestUser
	{
		$this->username = $username;

		return $this;
	}

	/**
	 * @return null|string
	 */
	public function getEmail(): ?string
	{
		return $this->email;
	}

	/**
	 * @param string|null $email
	 *
	 * @return RequestUser
	 */
	public function setEmail(string $email = null): RequestUser
	{
		$this->email = $email;

		return $this;
	}

	/**
	 * @return null|string
	 */
	public function getPassword(): ?string
	{
		return $this->password;
	}

	/**
	 * @param string|null $password
	 *
	 * @return RequestUser
	 */
	public function setPassword(string $password = null): RequestUser
	{
		$this->password = $password;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getName():? string
	{
		return $this->name;
	}

	/**
	 * @param string|null $name
	 *
	 * @return RequestUser
	 */
	public function setName(string $name = null): RequestUser
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getSurname(): ?string
	{
		return $this->surname;
	}

	/**
	 * @param string|null $surname
	 *
	 * @return RequestUser
	 */
	public function setSurname(string $surname = null): RequestUser
	{
		$this->surname = $surname;

		return $this;
	}

}