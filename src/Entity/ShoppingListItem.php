<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class ShoppingList
 * @package App\Entity
 *
 * @ORM\Entity(repositoryClass="App\Entity\Repository\ShoppingListItemRepository")
 * @ORM\Table()
 */
class ShoppingListItem implements ProductItemInterface
{
	/**
	 * @var int|null
	 *
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var Fridge|null
	 *
	 * @ORM\ManyToOne(targetEntity="App\Entity\Fridge", inversedBy="shoppingListItems", cascade={"PERSIST"})
	 * @ORM\JoinColumn(referencedColumnName="id")
	 */
	private $fridge;

	/**
	 * @var Product|null
	 *
	 * @ORM\ManyToOne(targetEntity="App\Entity\Product", inversedBy="shoppingListItems", cascade={"PERSIST"})
	 * @ORM\JoinColumn(referencedColumnName="id")
	 */
	protected $product;

	/**
	 * @var User|null
	 *
	 * @ORM\ManyToOne(targetEntity="App\Entity\User",  inversedBy="shoppingListItems", cascade={"PERSIST"})
	 * @ORM\JoinColumn(referencedColumnName="id")
	 */
	protected $createdBy;

	/**
	 * @var int|null
	 *
	 * @ORM\Column(type="integer", options={"default": 0})
	 */
	protected $amount;

	/**
	 * @var \DateTime|null
	 *
	 * @ORM\Column(type="datetime")
	 */
	protected $createdAt;

	/**
	 * @var string|null
	 *
	 * @ORM\Column(type="string", length=256, nullable=true)
	 */
	protected $description;

	/**
	 * @return int|null
	 */
	public function getId(): ?int
	{
		return $this->id;
	}

	/**
	 * @param int|null $id
	 *
	 * @return ShoppingListItem
	 */
	public function setId(?int $id): ShoppingListItem
	{
		$this->id = $id;

		return $this;
	}

	/**
	 * @return Fridge|null
	 */
	public function getFridge(): ?Fridge
	{
		return $this->fridge;
	}

	/**
	 * @param Fridge|null $fridge
	 *
	 * @return ShoppingListItem
	 */
	public function setFridge(?Fridge $fridge): ShoppingListItem
	{
		$this->fridge = $fridge;

		return $this;
	}

	/**
	 * @return Product|null
	 */
	public function getProduct(): ?Product
	{
		return $this->product;
	}

	/**
	 * @param Product $product
	 *
	 * @return ShoppingListItem
	 */
	public function setProduct(Product $product): ShoppingListItem
	{
		$this->product = $product;

		return $this;
	}

	/**
	 * @return User|null
	 */
	public function getCreatedBy(): ?User
	{
		return $this->createdBy;
	}

	/**
	 * @param User|null $createdBy
	 *
	 * @return ShoppingListItem
	 */
	public function setCreatedBy(?User $createdBy): ShoppingListItem
	{
		$this->createdBy = $createdBy;

		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getAmount(): ?int
	{
		return $this->amount;
	}

	/**
	 * @param int|null $amount
	 *
	 * @return ShoppingListItem
	 */
	public function setAmount(?int $amount): ShoppingListItem
	{
		$this->amount = $amount;

		return $this;
	}

	/**
	 * @return \DateTime|null
	 */
	public function getCreatedAt(): ?\DateTime
	{
		return $this->createdAt;
	}

	/**
	 * @param \DateTime|null $createdAt
	 *
	 * @return ShoppingListItem
	 */
	public function setCreatedAt(?\DateTime $createdAt): ShoppingListItem
	{
		$this->createdAt = $createdAt;

		return $this;
	}

	/**
	 * @return null|string
	 */
	public function getDescription(): ?string
	{
		return $this->description;
	}

	/**
	 * @param null|string $description
	 *
	 * @return ShoppingListItem
	 */
	public function setDescription(?string $description): ShoppingListItem
	{
		$this->description = $description;

		return $this;
	}

}