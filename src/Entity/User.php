<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity
 * @ORM\Table()
 */
class User extends BaseUser
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", length=64)
	 */
	protected $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", length=64)
	 */
	protected $surname;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", length=128, nullable=true)
	 */
	protected $avatar;

	/**
	 * @var UploadedFile
	 */
	protected $avatarFile;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\ManyToMany(targetEntity="App\Entity\Fridge")
	 * @ORM\JoinColumn(referencedColumnName="id")
	 */
	protected $fridges;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\OneToMany(targetEntity="App\Entity\Fridge", mappedBy="owner")
	 */
	protected $ownedFridges;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\OneToMany(targetEntity="App\Entity\FridgeInvitation", mappedBy="invitedUser")
	 */
	protected $invitations;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\OneToMany(targetEntity="App\Entity\ShoppingListItem", mappedBy="createdBy")
	 */
	private $shoppingListItems;

	/**
	 * User constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->fridges           = new ArrayCollection();
		$this->ownedFridges      = new ArrayCollection();
		$this->invitations       = new ArrayCollection();
		$this->shoppingListItems = new ArrayCollection();
	}

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 *
	 * @return User
	 */
	public function setName(string $name): User
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getSurname(): string
	{
		return $this->surname;
	}

	/**
	 * @param string $surname
	 *
	 * @return User
	 */
	public function setSurname(string $surname): User
	{
		$this->surname = $surname;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getAvatar()
	{
		return $this->avatar;
	}

	/**
	 * @param string $avatar
	 *
	 * @return User
	 */
	public function setAvatar($avatar): User
	{
		$this->avatar = $avatar;

		return $this;
	}

	/**
	 * @return UploadedFile
	 */
	public function getAvatarFile()
	{
		return $this->avatarFile;
	}

	/**
	 * @param UploadedFile $avatarFile
	 *
	 * @return User
	 */
	public function setAvatarFile(UploadedFile $avatarFile = null): User
	{
		$this->avatarFile = $avatarFile;

		return $this;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getFridges()
	{
		return $this->fridges;
	}

	/**
	 * @param Fridge $fridge
	 *
	 * @return ArrayCollection
	 */
	public function addFridge(Fridge $fridge)
	{
		if (false === $this->fridges->contains($fridge)) {
			$this->fridges->add($fridge);
			$fridge->addMember($this);
		}

		return $this->fridges;
	}

	/**
	 * @param Fridge $fridge
	 *
	 * @return ArrayCollection
	 */
	public function removeFridge(Fridge $fridge)
	{
		if (true === $this->fridges->contains($fridge)) {
			$this->fridges->removeElement($fridge);
			$fridge->removeMember($this);
		}

		return $this->fridges;
	}

	/**
	 * @param Fridge $fridge
	 *
	 * @return ArrayCollection
	 */
	public function addOwnedFridge(Fridge $fridge)
	{
		if (false === $this->ownedFridges->contains($fridge)) {
			$this->ownedFridges->add($fridge);
			$fridge->setOwner($this);
		}

		return $this->fridges;
	}

	/**
	 * @param Fridge $fridge
	 *
	 * @return ArrayCollection
	 */
	public function removeOwnedFridge(Fridge $fridge)
	{
		if (true === $this->ownedFridges->contains($fridge)) {
			$this->ownedFridges->removeElement($fridge);
			$fridge->setOwner(null);
		}

		return $this->fridges;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getOwnedFridges()
	{
		return $this->ownedFridges;
	}

	/**
	 * @param FridgeInvitation $fridgeInvitation
	 *
	 * @return ArrayCollection
	 */
	public function addInvitation(FridgeInvitation $fridgeInvitation)
	{
		if (false === $this->invitations->contains($fridgeInvitation)) {
			$this->invitations->add($fridgeInvitation);
			$fridgeInvitation->setInvitedUser($this);
		}

		return $this->invitations;
	}

	/**
	 * @param FridgeInvitation $fridgeInvitation
	 *
	 * @return ArrayCollection
	 */
	public function removeInvitation(FridgeInvitation $fridgeInvitation)
	{
		if (true === $this->invitations->contains($fridgeInvitation)) {
			$this->invitations->removeElement($fridgeInvitation);
			$fridgeInvitation->setInvitedUser(null);
		}

		return $this->invitations;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getInvitations()
	{
		return $this->invitations;
	}

	/**
	 * @param ShoppingListItem $shoppingListItem
	 *
	 * @return User|null
	 */
	public function addShoppingListItem(ShoppingListItem $shoppingListItem): ?User
	{
		if (false === $this->shoppingListItems->contains($shoppingListItem)) {
			$this->shoppingListItems->add($shoppingListItem);
			$shoppingListItem->setCreatedBy($this);
		}

		return $this;
	}

	/**
	 * @param ShoppingListItem $shoppingListItem
	 *
	 * @return User|null
	 */
	public function removeShoppingListItem(ShoppingListItem $shoppingListItem): ?User
	{
		if (true === $this->shoppingListItems->contains($shoppingListItem)) {
			$this->shoppingListItems->removeElement($shoppingListItem);
			$shoppingListItem->setCreatedBy(null);
		}

		return $this;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getShoppingListItems()
	{
		return $this->shoppingListItems;
	}
}
