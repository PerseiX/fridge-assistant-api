<?php

namespace App\EventListener;

use ApiBundle\Event\FormRepresentationEvent;
use App\Entity\FridgeInvitation;
use App\Representation\FridgeInvitationRepresentation;
use App\Representation\FridgeItemRepresentation;
use App\Representation\ShoppingListItemRepresentation;
use JMS\Serializer\Serializer;
use Pusher\Pusher;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class FormResponseListener
 * @package App\EventListener
 */
class FormResponseListener implements EventSubscriberInterface
{
	/**
	 * @var Pusher
	 */
	private $pusher;

	/**
	 * @var Serializer
	 */
	private $jmsSerializer;

	/**
	 * FormResponseListener constructor.
	 *
	 * @param Pusher     $pusher
	 * @param Serializer $jmsSerializer
	 */
	public function __construct(Pusher $pusher, Serializer $jmsSerializer)
	{
		$this->pusher        = $pusher;
		$this->jmsSerializer = $jmsSerializer;
	}

	/**
	 * @return array
	 */
	public static function getSubscribedEvents()
	{
		return [
			FormRepresentationEvent::POST_SUCCESS_REPRESENTATION => [
				'onPostSuccessRepresentation'
			]
		];
	}

	/**
	 * @param FormRepresentationEvent $event
	 *
	 * Generic approve force implement in event
	 *
	 * @throws \Pusher\PusherException
	 */
	public function onPostSuccessRepresentation(FormRepresentationEvent $event)
	{
		$representation = $event->getObject();

		if ($representation instanceof FridgeInvitationRepresentation) {
			$pusher          = $this->pusher;
			$jsonInvitation  = $this->jmsSerializer->serialize($representation, 'json');
			$data['message'] = $jsonInvitation;
			$pusher->trigger('invitations', 'invite-user-' . $representation->getInvitedUser()->getId(), $data);
		}

		if ($representation instanceof FridgeItemRepresentation) {
			$output          = $this->jmsSerializer->serialize($representation, 'json');
			$data['message'] = $output;
			$this->pusher->trigger('fridge', 'edit-product-item-fridge-' . $representation->getFridge()->getId(), $data);
		}

		if ($representation instanceof ShoppingListItemRepresentation) {
			$output          = $this->jmsSerializer->serialize($representation, 'json');
			$data['message'] = $output;
			$this->pusher->trigger('shoppingList', 'edit-shopping-list-item-fridge-' . $representation->getFridge()->getId(), $data);
		}
	}
}