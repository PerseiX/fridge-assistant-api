<?php

namespace App\EventListener;

use App\Entity\User;
use App\Resolver\ImagePathResolver;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class JWTCreatedListener
 * @package App\EventListener
 */
class JWTCreatedListener
{

	/**
	 * @var RequestStack
	 */
	private $requestStack;

	/**
	 * @var ImagePathResolver
	 */
	private $imagePathResolver;

	/**
	 * JWTCreatedListener constructor.
	 *
	 * @param RequestStack      $requestStack
	 * @param ImagePathResolver $imagePathResolver
	 */
	public function __construct(RequestStack $requestStack, ImagePathResolver $imagePathResolver)
	{
		$this->requestStack      = $requestStack;
		$this->imagePathResolver = $imagePathResolver;
	}

	/**
	 * @param JWTCreatedEvent $event
	 *
	 * @return void
	 */
	public function onJWTCreated(JWTCreatedEvent $event)
	{

		/** @var User $user */
		$user    = $event->getUser();
		$payload = [
			'id' => $user->getId(),
			'username' => $user->getUsername(),
			'avatar'   => $this->imagePathResolver->resolve($user->getAvatar(), '/avatar')
		];
		$event->setData($payload);
	}
}