<?php

namespace App\EventListener;

use ApiBundle\Event\FormRepresentationEvent;
use App\Representation\UserRepresentation;
use FOS\UserBundle\Model\UserInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Http\Authentication\AuthenticationSuccessHandler;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Class PostSuccessUpdateUser
 * @package App\EventListener
 */
class PostSuccessUpdateUserListener
{
	/**
	 * @var TokenStorage
	 */
	private $tokenStorage;

	/**
	 * @var JWTManager
	 */
	private $jwtManager;

	/**
	 * @var AuthenticationSuccessHandler
	 */
	private $jwtAuthenticationHandler;

	/**
	 * PostSuccessUpdateUserListener constructor.
	 *
	 * @param TokenStorage                 $tokenStorage
	 * @param JWTManager                   $jwtManager
	 * @param AuthenticationSuccessHandler $jwtAuthenticationHandler
	 */
	public function __construct(TokenStorage $tokenStorage, JWTManager $jwtManager, AuthenticationSuccessHandler $jwtAuthenticationHandler)
	{
		$this->tokenStorage             = $tokenStorage;
		$this->jwtManager               = $jwtManager;
		$this->jwtAuthenticationHandler = $jwtAuthenticationHandler;
	}

	public function onPostUpdate(FormRepresentationEvent $formRepresentationEvent)
	{
		$object = $formRepresentationEvent->getObject();
		/** @var UserRepresentation $object */
		if ($object instanceof UserRepresentation) {
			/** @var UserInterface $loggedUser */
			$loggedUser = $this->tokenStorage->getToken()->getUser();
			$jwt        = $this->jwtManager->create($loggedUser);
			$this->jwtAuthenticationHandler->handleAuthenticationSuccess($loggedUser, $jwt);

			$object->setToken($jwt);
		}
	}
}