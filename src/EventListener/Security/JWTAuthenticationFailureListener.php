<?php

namespace App\EventListener\Security;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationFailureEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Response\JWTAuthenticationFailureResponse;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class JWYAuthenticationFailureListener
 * @package App\EventListener\Security
 */
class JWTAuthenticationFailureListener
{
	/**
	 * @var TranslatorInterface
	 */
	private $translator;

	/**
	 * JWTAuthenticationFailureListener constructor.
	 *
	 * @param TranslatorInterface $translator
	 */
	public function __construct(TranslatorInterface $translator)
	{
		$this->translator = $translator;
	}

	public function onJWTAuthenticationFailure(AuthenticationFailureEvent $event)
	{
		/** @var JWTAuthenticationFailureResponse $response */
		$response = $event->getResponse();
		$response->setMessage($this->translator->trans('Bad Credentials'));
	}
}