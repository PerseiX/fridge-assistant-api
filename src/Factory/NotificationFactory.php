<?php

namespace App\Factory;

use App\Resolver\ImagePathResolver;
use App\Model\NotificationContext;
use App\Model\Notification;
use App\Entity\User;

/**
 * Class NotificationBuilder
 * @package App\Builder
 */
class NotificationFactory
{
	/**
	 * @var ImagePathResolver
	 */
	private $imageResolver;

	/**
	 * NotificationFactory constructor.
	 *
	 * @param ImagePathResolver $imageResolver
	 */
	public function __construct(ImagePathResolver $imageResolver)
	{
		$this->imageResolver = $imageResolver;
	}

	/**
	 * @param NotificationContext $context
	 * @param string              $action
	 *
	 * @return Notification
	 * @throws \Exception
	 */
	public function createNotification(NotificationContext $context, string $action)
	{
		$notification = new Notification();
		$user         = $context->getUser();
		$productItem  = $context->getProductItem();
		$notification
			->setAvatar($this->imageResolver->resolve($user->getAvatar(), '/avatar'))
			->setAction($action)
			->setFridgeSlug($productItem->getFridge()->getSlug())
			->setUsername($user->getUsername())
			->setProduct($productItem->getProduct()->getName())
			->setCreatedAt(new \DateTime())
			->setContext($context->getContext());

		/** @var User $item */
		foreach (array_merge($productItem->getFridge()->getMembers()->getValues(), [$productItem->getFridge()->getOwner()]) as $item) {
			$notification->addFridgeMemberId($item->getId());
		}

		return $notification;
	}
}