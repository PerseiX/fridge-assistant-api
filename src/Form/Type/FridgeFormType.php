<?php

namespace App\Form\Type;

use App\Entity\Fridge;
use App\Entity\FridgeInvitation;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class FridgeFormType
 * @package App\Form\Type
 */
class FridgeFormType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('name', TextType::class)
			->add('active', CheckboxType::class)
//			->add('owner', EntityType::class, [
//				'class' => Fridge::class
//			])
			->add('members', CollectionType::class, [
				'entry_type'    => EntityType::class,
				'entry_options' => [
					'class' => Fridge::class
				]
			]);
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'csrf_protection' => false,
			'data_class'      => Fridge::class
		]);
	}

	public function getBlockPrefix()
	{
		return "form_fridge";
	}
}