<?php

namespace App\Form\Type;

use App\Entity\Fridge;
use App\Entity\FridgeInvitation;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class FridgeInvitationType
 * @package App\Form
 */
class FridgeInvitationFormType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array                $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('invitedUser', EntityType::class, [
				'class'        => User::class,
				'choice_value' => 'email',
			])
			->add('invitedFridge', EntityType::class, [
				'class' => Fridge::class
			])
			->add('accepted', CheckboxType::class);
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'csrf_protection'    => false,
			'data_class'         => FridgeInvitation::class,
			'allow_extra_fields' => true
		]);
	}

	public function getBlockPrefix()
	{
		return "form_invitation_type";
	}
}