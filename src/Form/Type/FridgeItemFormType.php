<?php

namespace App\Form\Type;

use App\Entity\FridgeItem;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;

/**
 * Class ShoppingListItemType
 * @package App\Form\Type
 */
class FridgeItemFormType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array                $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('description', TextType::class)
			->add('amount', NumberType::class);
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'csrf_protection' => false,
			'data_class'      => FridgeItem::class
		]);
	}

	public function getBlockPrefix()
	{
		return "form_fridge_item";
	}
}