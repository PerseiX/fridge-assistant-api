<?php

namespace App\Form\Type;

use ApiBundle\Form\Type\PerseiFileType;
use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UserFormType
 * @package App\Form\Type
 */
class UserFormType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('username', TextType::class)
			->add('email', EmailType::class)
			->add('name', TextType::class)
			->add('surname', TextType::class)
			->add('avatarFile', PerseiFileType::class, [
				'property'   => 'avatar',
				'uploadPath' => 'avatar'
			]);
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'csrf_protection' => false,
			'data_class'      => User::class
		]);
	}

	public function getBlockPrefix()
	{
		return "form_user";
	}
}