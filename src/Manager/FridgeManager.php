<?php

namespace App\Manager;

use App\Entity\Fridge;
use App\Entity\FridgeItem;
use App\Entity\Product;
use App\Entity\User;
use Doctrine\ORM\EntityManager;

/**
 * Class FridgeManager
 * @package App\Manager
 */
class FridgeManager
{
	/**
	 * @var EntityManager
	 */
	private $em;

	/**
	 * FridgeManager constructor.
	 *
	 * @param EntityManager $em
	 */
	public function __construct(EntityManager $em)
	{
		$this->em = $em;
	}

	/**
	 * @param Product $product
	 * @param Fridge  $fridge
	 * @param User    $user
	 *
	 * @return FridgeItem
	 * @throws \Doctrine\ORM\ORMException
	 * @throws \Doctrine\ORM\OptimisticLockException
	 */
	public function addFridgeItem(Product $product, Fridge $fridge, User $user)
	{
		$fridgeItem = new FridgeItem();
		$fridgeItem
			->setCreatedAt(new \DateTime())
			->setProduct($product)
			->setFridge($fridge)
			->setCreatedBy($user)
			->setAmount(1);

		$this->em->persist($fridgeItem);
		$this->em->flush();

		return $fridgeItem;
	}
}