<?php

namespace App\Manager;

use App\Entity\FridgeInvitation;
use Doctrine\ORM\EntityManager;

/**
 * Class InvitationManager
 * @package App\Manager
 */
class InvitationManager
{
	/**
	 * @var EntityManager
	 */
	private $em;

	/**
	 * InvitationManager constructor.
	 *
	 * @param EntityManager $em
	 */
	public function __construct(EntityManager $em)
	{
		$this->em = $em;
	}

	/**
	 * @param FridgeInvitation $fridgeInvitation
	 *
	 * @return \App\Entity\Fridge|null
	 * @throws \Doctrine\ORM\ORMException
	 * @throws \Doctrine\ORM\OptimisticLockException
	 */
	public function applyInvitation(FridgeInvitation $fridgeInvitation)
	{
		$fridge      = $fridgeInvitation->getInvitedFridge();
		$invitedUser = $fridgeInvitation->getInvitedUser();
		$fridge->addMember($invitedUser);
		$this->em->remove($fridgeInvitation);
		$this->em->persist($fridge);
		$this->em->flush();

		return $fridge;
	}

	/**
	 * @param FridgeInvitation $fridgeInvitation
	 *
	 * @return \App\Entity\Fridge|null
	 * @throws \Doctrine\ORM\ORMException
	 * @throws \Doctrine\ORM\OptimisticLockException
	 */
	public function rejectInvitation(FridgeInvitation $fridgeInvitation)
	{
		$this->em->remove($fridgeInvitation);
		$this->em->flush();

		return $fridgeInvitation->getInvitedFridge();
	}
}