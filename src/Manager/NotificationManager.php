<?php

namespace App\Manager;

use App\Entity\FridgeItem;
use App\Entity\ProductItemInterface;
use App\Entity\User;
use App\Factory\NotificationFactory;
use App\Model\Notification;
use App\Model\NotificationContext;
use App\Pusher\Handlers\ProductHandler;
use App\Utility\KeyGenerator;
use FOS\UserBundle\Model\UserInterface;
use Predis\Client;

/**
 * Class NotificationManager
 * @package App\Manager
 */
class NotificationManager
{
	const CREATE = 'CREATE';
	const EDIT   = 'EDIT';
	const DELETE = 'DELETE';

	/**
	 * @var Client
	 */
	private $client;

	/**
	 * @var NotificationFactory
	 */
	private $notificationFactory;

	/**
	 * @var ProductHandler
	 */
	private $productHandler;

	/**
	 * NotificationManager constructor.
	 *
	 * @param Client              $client
	 * @param NotificationFactory $notificationFactory
	 * @param ProductHandler      $productHandler
	 */
	public function __construct(Client $client, NotificationFactory $notificationFactory, ProductHandler $productHandler)
	{
		$this->client              = $client;
		$this->notificationFactory = $notificationFactory;
		$this->productHandler      = $productHandler;
	}

	/**
	 * @param NotificationContext $context
	 *
	 * @throws \Exception
	 */
	public function createAddNotification(NotificationContext $context)
	{
		$notification = $this->notificationFactory->createNotification($context, self::CREATE);
		$productItem  = $context->getProductItem();
		$times        = time();

		/** @var User $member */
		foreach ($notification->getFridgeMembersId() as $memberId) {
			$key = sprintf('%s_create_%s_%s', $memberId, $productItem->getId(), $times);
			$notification->setKey($key);
			$this->client->set($key, serialize($notification));
		}

		$this->productHandler->addProductNotification($notification);
	}

	/**
	 * @param NotificationContext $context
	 *
	 * @throws \Exception
	 */
	public function createEditNotification(NotificationContext $context)
	{
		$notification = $this->notificationFactory->createNotification($context, self::EDIT);
		$productItem  = $context->getProductItem();
		$times        = time();

		/** @var User $member */
		foreach ($notification->getFridgeMembersId() as $memberId) {
			$key = sprintf('%s_edit_%s_%s', $memberId, $productItem->getId(), $times);
			$notification->setKey($key);
			$this->client->set($key, serialize($notification));
		}

		$this->productHandler->editProductNotification($notification);
	}

	/**
	 * @param NotificationContext $context
	 *
	 * @throws \Exception
	 */
	public function createDeleteNotification(NotificationContext $context)
	{
		$notification = $this->notificationFactory->createNotification($context, self::DELETE);
		$productItem  = $context->getProductItem();
		$times        = time();

		/** @var User $member */
		foreach ($notification->getFridgeMembersId() as $memberId) {
			$key = sprintf('%s_delete_%s_%s', $memberId, $productItem->getId(), $times);
			$notification->setKey($key);
			$this->client->set($key, serialize($notification));
		}

		$this->productHandler->deleteProductNotification($notification);
	}

	/**
	 * @param string $key
	 *
	 * @return string
	 */
	public function deleteNotification(string $key)
	{
		$notification = $this->client->get($key);
		$this->client->del([$key]);

		return $notification;
	}

	/**
	 * @param UserInterface $user
	 * @param string|null   $type
	 *
	 * @return array
	 */
	public function getNotifications(UserInterface $user, string $type = null)
	{
		$key  = KeyGenerator::generateKey($user->getId(), $type);
		$keys = $this->client->keys($key);

		$collection = [];
		foreach ($keys as $item) {
			$collection[] = $this->client->get($item);
		}

		return $collection;
	}

	/**
	 * @param UserInterface $user
	 *
	 * @return bool
	 */
	public function hasNotification(UserInterface $user)
	{
		$key  = KeyGenerator::generateKey($user->getId());
		$keys = $this->client->keys($key);

		return count($keys) > 0;
	}
}