<?php

namespace App\Manager;

use App\Entity\Fridge;
use App\Entity\Product;
use App\Entity\ShoppingListItem;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Model\UserInterface;

/**
 * Class ShoppingListManager
 * @package App\Manager
 */
class ShoppingListManager
{
	/**
	 * @var EntityManager
	 */
	private $em;

	/**
	 * FridgeManager constructor.
	 *
	 * @param EntityManager $em
	 */
	public function __construct(EntityManager $em)
	{
		$this->em = $em;
	}

	/**
	 * @param Product       $product
	 * @param Fridge        $fridge
	 * @param UserInterface $user
	 *
	 * @return ShoppingListItem
	 * @throws \Doctrine\ORM\ORMException
	 * @throws \Doctrine\ORM\OptimisticLockException
	 */
	public function addShoppingListItem(Product $product, Fridge $fridge, UserInterface $user)
	{
		$shoppingListItem = new ShoppingListItem();
		$shoppingListItem
			->setCreatedAt(new \DateTime())
			->setProduct($product)
			->setFridge($fridge)
			->setCreatedBy($user)
			->setAmount(1);

		$this->em->persist($shoppingListItem);
		$this->em->flush();

		return $shoppingListItem;
	}
}