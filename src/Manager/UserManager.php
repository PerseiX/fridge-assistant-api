<?php

namespace App\Manager;

use App\Entity\RequestUser;
use App\Entity\User;
use App\Transformer\RequestUserTransformer;
use Doctrine\Common\Persistence\ObjectManager;
use FOS\UserBundle\Model\GroupManager;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Util\UserManipulator;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class RegisterManager
 * @package App\Manager
 */
class UserManager
{
	/**
	 * @var RequestUserTransformer
	 */
	private $requestUserTransformer;

	/**
	 * @var ObjectManager
	 */
	private $em;

	/**
	 * @var ValidatorInterface;
	 */
	private $validation;

	/**
	 * @var EncoderFactory
	 */
	private $encoderFactory;

	/**
	 * @var \FOS\UserBundle\Model\UserManager
	 */
	private $userManager;

	/**
	 * UserManager constructor.
	 *
	 * @param RequestUserTransformer            $requestUserTransformer
	 * @param ObjectManager                     $em
	 * @param ValidatorInterface                $validation
	 * @param EncoderFactory                    $encoderFactory
	 * @param \FOS\UserBundle\Model\UserManager $userManager
	 */
	public function __construct(RequestUserTransformer $requestUserTransformer, ObjectManager $em, ValidatorInterface $validation, EncoderFactory $encoderFactory, \FOS\UserBundle\Model\UserManager $userManager)
	{
		$this->requestUserTransformer = $requestUserTransformer;
		$this->em                     = $em;
		$this->validation             = $validation;
		$this->encoderFactory         = $encoderFactory;
		$this->userManager            = $userManager;
	}

	/**
	 * @param RequestUser $requestUser
	 *
	 * @return User|array
	 */
	public function registry(RequestUser $requestUser)
	{
		$errors = $this->validation->validate($requestUser);

		if (count($errors) > 0) {
			$messages = [];
			foreach ($errors as $error) {
				$key            = $error->getPropertyPath() ? $error->getPropertyPath() : $error->getConstraint()->field;
				$messages['message'][$key] = $error->getMessage();
			}

			return $messages;
		}

		$user = $this->requestUserTransformer->transform($requestUser);

		$this->em->persist($user);
		$this->em->flush();

		return $user;
	}

	/**
	 * @param UserInterface $user
	 * @param string        $password
	 * @param string        $newPassword
	 *
	 * @return UserInterface
	 * @throws \Exception
	 */
	public function resetPassword(UserInterface $user, string $password, string $newPassword)
	{

		$encoderFactory = $this->encoderFactory->getEncoder($user);
		$hashedPassword = $encoderFactory->encodePassword($password, $user->getSalt());

		if ($hashedPassword !== $user->getPassword()) {
			throw new \Exception("Passed password is incorrect");
		}
		$user->setPlainPassword($newPassword);
		$this->userManager->updatePassword($user);
		$this->em->flush();

		return $user;
	}
}