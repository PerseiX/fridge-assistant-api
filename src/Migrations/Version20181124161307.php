<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181124161307 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE fridge_invitation (id INT AUTO_INCREMENT NOT NULL, invited_user_id INT DEFAULT NULL, invited_fridge_id INT DEFAULT NULL, accepted TINYINT(1) NOT NULL, invited_at DATETIME NOT NULL, INDEX IDX_DE653257C58DAD6E (invited_user_id), INDEX IDX_DE653257545188F7 (invited_fridge_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fridge (id INT AUTO_INCREMENT NOT NULL, owner_id INT DEFAULT NULL, name VARCHAR(128) NOT NULL, active TINYINT(1) NOT NULL, slug VARCHAR(128) DEFAULT NULL, created_at DATETIME DEFAULT NULL, INDEX IDX_F2E94D897E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, username_canonical VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, email_canonical VARCHAR(180) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, confirmation_token VARCHAR(180) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', name VARCHAR(64) NOT NULL, surname VARCHAR(64) NOT NULL, avatar VARCHAR(128) DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D64992FC23A8 (username_canonical), UNIQUE INDEX UNIQ_8D93D649A0D96FBF (email_canonical), UNIQUE INDEX UNIQ_8D93D649C05FB297 (confirmation_token), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_fridge (user_id INT NOT NULL, fridge_id INT NOT NULL, INDEX IDX_97BB56B4A76ED395 (user_id), INDEX IDX_97BB56B414A48E59 (fridge_id), PRIMARY KEY(user_id, fridge_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(512) NOT NULL, description VARCHAR(512) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_fridge (product_id INT NOT NULL, fridge_id INT NOT NULL, INDEX IDX_90950C2D4584665A (product_id), INDEX IDX_90950C2D14A48E59 (fridge_id), PRIMARY KEY(product_id, fridge_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fridge_item (id INT AUTO_INCREMENT NOT NULL, fridge_id INT DEFAULT NULL, product_id INT DEFAULT NULL, created_by_id INT DEFAULT NULL, amount INT DEFAULT 0 NOT NULL, created_at DATETIME NOT NULL, description VARCHAR(256) DEFAULT NULL, INDEX IDX_170F6F1614A48E59 (fridge_id), INDEX IDX_170F6F164584665A (product_id), INDEX IDX_170F6F16B03A8386 (created_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE shopping_list_item (id INT AUTO_INCREMENT NOT NULL, fridge_id INT DEFAULT NULL, product_id INT DEFAULT NULL, created_by_id INT DEFAULT NULL, amount INT DEFAULT 0 NOT NULL, created_at DATETIME NOT NULL, description VARCHAR(256) DEFAULT NULL, INDEX IDX_4FB1C22414A48E59 (fridge_id), INDEX IDX_4FB1C2244584665A (product_id), INDEX IDX_4FB1C224B03A8386 (created_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE fridge_invitation ADD CONSTRAINT FK_DE653257C58DAD6E FOREIGN KEY (invited_user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE fridge_invitation ADD CONSTRAINT FK_DE653257545188F7 FOREIGN KEY (invited_fridge_id) REFERENCES fridge (id)');
        $this->addSql('ALTER TABLE fridge ADD CONSTRAINT FK_F2E94D897E3C61F9 FOREIGN KEY (owner_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_fridge ADD CONSTRAINT FK_97BB56B4A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_fridge ADD CONSTRAINT FK_97BB56B414A48E59 FOREIGN KEY (fridge_id) REFERENCES fridge (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_fridge ADD CONSTRAINT FK_90950C2D4584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_fridge ADD CONSTRAINT FK_90950C2D14A48E59 FOREIGN KEY (fridge_id) REFERENCES fridge (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE fridge_item ADD CONSTRAINT FK_170F6F1614A48E59 FOREIGN KEY (fridge_id) REFERENCES fridge (id)');
        $this->addSql('ALTER TABLE fridge_item ADD CONSTRAINT FK_170F6F164584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE fridge_item ADD CONSTRAINT FK_170F6F16B03A8386 FOREIGN KEY (created_by_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE shopping_list_item ADD CONSTRAINT FK_4FB1C22414A48E59 FOREIGN KEY (fridge_id) REFERENCES fridge (id)');
        $this->addSql('ALTER TABLE shopping_list_item ADD CONSTRAINT FK_4FB1C2244584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE shopping_list_item ADD CONSTRAINT FK_4FB1C224B03A8386 FOREIGN KEY (created_by_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fridge_invitation DROP FOREIGN KEY FK_DE653257545188F7');
        $this->addSql('ALTER TABLE user_fridge DROP FOREIGN KEY FK_97BB56B414A48E59');
        $this->addSql('ALTER TABLE product_fridge DROP FOREIGN KEY FK_90950C2D14A48E59');
        $this->addSql('ALTER TABLE fridge_item DROP FOREIGN KEY FK_170F6F1614A48E59');
        $this->addSql('ALTER TABLE shopping_list_item DROP FOREIGN KEY FK_4FB1C22414A48E59');
        $this->addSql('ALTER TABLE fridge_invitation DROP FOREIGN KEY FK_DE653257C58DAD6E');
        $this->addSql('ALTER TABLE fridge DROP FOREIGN KEY FK_F2E94D897E3C61F9');
        $this->addSql('ALTER TABLE user_fridge DROP FOREIGN KEY FK_97BB56B4A76ED395');
        $this->addSql('ALTER TABLE fridge_item DROP FOREIGN KEY FK_170F6F16B03A8386');
        $this->addSql('ALTER TABLE shopping_list_item DROP FOREIGN KEY FK_4FB1C224B03A8386');
        $this->addSql('ALTER TABLE product_fridge DROP FOREIGN KEY FK_90950C2D4584665A');
        $this->addSql('ALTER TABLE fridge_item DROP FOREIGN KEY FK_170F6F164584665A');
        $this->addSql('ALTER TABLE shopping_list_item DROP FOREIGN KEY FK_4FB1C2244584665A');
        $this->addSql('DROP TABLE fridge_invitation');
        $this->addSql('DROP TABLE fridge');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_fridge');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE product_fridge');
        $this->addSql('DROP TABLE fridge_item');
        $this->addSql('DROP TABLE shopping_list_item');
    }
}
