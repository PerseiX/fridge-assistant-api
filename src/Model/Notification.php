<?php

namespace App\Model;

/**
 * Class Notification
 * @package App\Model
 */
class Notification
{
	const PRODUCT_CONTEXT       = 'product';
	const SHOPPING_LIST_CONTEXT = 'shopping-list';

	/**
	 * @var string
	 */
	private $key;

	/**
	 * @var string
	 */
	private $username;

	/**
	 * @var string
	 */
	private $avatar;

	/**
	 * @var string
	 */
	private $action;

	/**
	 * @var string
	 */
	private $fridgeSlug;

	/**
	 * @var string
	 */
	private $product;

	/**
	 * @var \DateTime
	 */
	private $createdAt;

	/**
	 * @var
	 */
	private $fridgeMembersId;

	/**
	 * @var string
	 */
	private $context;

	/**
	 * Notification constructor.
	 */
	public function __construct()
	{
		$this->fridgeMembersId = [];
	}

	/**
	 * @return string
	 */
	public function getKey(): string
	{
		return $this->key;
	}

	/**
	 * @param string $key
	 *
	 * @return Notification
	 */
	public function setKey(string $key): Notification
	{
		$this->key = $key;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getUsername(): string
	{
		return $this->username;
	}

	/**
	 * @param string $username
	 *
	 * @return Notification
	 */
	public function setUsername(string $username): Notification
	{
		$this->username = $username;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getAvatar(): ?string
	{
		return $this->avatar;
	}

	/**
	 * @param string $avatar
	 *
	 * @return Notification
	 */
	public function setAvatar(?string $avatar): Notification
	{
		$this->avatar = $avatar;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getAction(): string
	{
		return $this->action;
	}

	/**
	 * @param string $action
	 *
	 * @return Notification
	 */
	public function setAction(string $action): Notification
	{
		$this->action = $action;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getFridgeSlug(): string
	{
		return $this->fridgeSlug;
	}

	/**
	 * @param string $fridgeSlug
	 *
	 * @return Notification
	 */
	public function setFridgeSlug(string $fridgeSlug): Notification
	{
		$this->fridgeSlug = $fridgeSlug;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getProduct(): string
	{
		return $this->product;
	}

	/**
	 * @param string $product
	 *
	 * @return Notification
	 */
	public function setProduct(string $product): Notification
	{
		$this->product = $product;

		return $this;
	}

	/**
	 * @param int $id
	 *
	 * @return $this
	 */
	public function addFridgeMemberId(int $id): Notification
	{
		$this->fridgeMembersId[] = $id;

		return $this;
	}

	/**
	 * @return array
	 */
	public function getFridgeMembersId(): array
	{
		return $this->fridgeMembersId;
	}

	/**
	 * @return \DateTime
	 */
	public function getCreatedAt(): ?\DateTime
	{
		return $this->createdAt;
	}

	/**
	 * @param \DateTime $createdAt
	 *
	 * @return Notification
	 */
	public function setCreatedAt(\DateTime $createdAt = null): Notification
	{
		$this->createdAt = $createdAt;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getContext(): string
	{
		return $this->context;
	}

	/**
	 * @param string $context
	 *
	 * @return Notification
	 */
	public function setContext(string $context): Notification
	{
		$this->context = $context;

		return $this;
	}

}