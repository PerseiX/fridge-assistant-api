<?php

namespace App\Model;

use App\Entity\ProductItemInterface;
use FOS\UserBundle\Model\UserInterface;

/**
 * Class NotificationContext
 * @package App\Model
 */
class NotificationContext
{
	/**
	 * @var UserInterface
	 */
	private $user;

	/**
	 * @var ProductItemInterface
	 */
	private $productItem;

	/**
	 * @var string
	 */
	private $context;

	/**
	 * NotificationContext constructor.
	 *
	 * @param UserInterface        $user
	 * @param ProductItemInterface $productItem
	 * @param string               $context
	 */
	public function __construct(UserInterface $user, ProductItemInterface $productItem, string $context)
	{
		$this->user        = $user;
		$this->productItem = $productItem;
		$this->context     = $context;
	}

	/**
	 * @return UserInterface
	 */
	public function getUser(): UserInterface
	{
		return $this->user;
	}

	/**
	 * @param UserInterface $user
	 *
	 * @return NotificationContext
	 */
	public function setUser(UserInterface $user): NotificationContext
	{
		$this->user = $user;

		return $this;
	}

	/**
	 * @return ProductItemInterface
	 */
	public function getProductItem(): ProductItemInterface
	{
		return $this->productItem;
	}

	/**
	 * @param ProductItemInterface $productItem
	 *
	 * @return NotificationContext
	 */
	public function setProductItem(ProductItemInterface $productItem): NotificationContext
	{
		$this->productItem = $productItem;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getContext(): string
	{
		return $this->context;
	}

	/**
	 * @param string $context
	 *
	 * @return NotificationContext
	 */
	public function setContext(string $context): NotificationContext
	{
		$this->context = $context;

		return $this;
	}

}