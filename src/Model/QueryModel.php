<?php

namespace App\Model;

use App\Entity\User;

/**
 * Class QueryModel
 * @package App\Model
 */
class QueryModel
{
	/**
	 * @var User
	 */
	private $user;

	/**
	 * @var string
	 */
	private $phrase;

	/**
	 * @var int
	 */
	private $fridgeId;

	/**
	 * QueryModel constructor.
	 *
	 * @param User   $user
	 * @param string $phrase
	 * @param int    $fridgeId
	 */
	public function __construct(User $user, string $phrase, int $fridgeId)
	{
		$this->user     = $user;
		$this->phrase   = $phrase;
		$this->fridgeId = $fridgeId;
	}

	/**
	 * @return User
	 */
	public function getUser(): User
	{
		return $this->user;
	}

	/**
	 * @return string
	 */
	public function getPhrase(): ?string
	{
		return $this->phrase;
	}

	/**
	 * @return int
	 */
	public function getFridgeId(): int
	{
		return $this->fridgeId;
	}

}