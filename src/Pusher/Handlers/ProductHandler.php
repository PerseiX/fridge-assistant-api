<?php

namespace App\Pusher\Handlers;

use App\Entity\FridgeItem;
use App\Model\Notification;
use App\Representation\FridgeItemRepresentation;
use App\Representation\FridgeRepresentation;
use JMS\Serializer\Serializer;
use Pusher\Pusher;

/**
 * Class FridgeHandler
 * @package App\Pusher\Handlers
 */
class ProductHandler
{
	/**
	 * @var Pusher
	 */
	private $pusher;

	/**
	 * @var Serializer
	 */
	private $jmsSerializer;

	/**
	 * FridgeHandler constructor.
	 *
	 * @param Pusher     $pusher
	 * @param Serializer $jmsSerializer
	 */
	public function __construct(Pusher $pusher, Serializer $jmsSerializer)
	{
		$this->pusher        = $pusher;
		$this->jmsSerializer = $jmsSerializer;
	}

	/**
	 * @param FridgeItemRepresentation $representation
	 *
	 * @throws \Pusher\PusherException
	 */
	public function removeProductFromFridge(FridgeItemRepresentation $representation)
	{
		$jsonInvitation  = $this->jmsSerializer->serialize($representation, 'json');
		$data['message'] = $jsonInvitation;
		$this->pusher->trigger('fridge', 'remove-product-fridge-' . $representation->getFridge()->getId(), $data);
	}

	/**
	 * @param FridgeItemRepresentation $representation
	 *
	 * @throws \Pusher\PusherException
	 */
	public function addProductFromFridge(FridgeItemRepresentation $representation)
	{
		$jsonInvitation  = $this->jmsSerializer->serialize($representation, 'json');
		$data['message'] = $jsonInvitation;
		$this->pusher->trigger('fridge', 'add-product-fridge-' . $representation->getFridge()->getId(), $data);
	}

	public function addProductNotification(Notification $notification)
	{
		$notificationJson = $this->jmsSerializer->serialize($notification, 'json');
		$data['message']  = $notificationJson;

		foreach ($notification->getFridgeMembersId() as $memberId) {
			$this->pusher->trigger('notifications', 'add-product-fridge-' . $memberId, $data);
		}
	}

	public function editProductNotification(Notification $notification)
	{
		$notificationJson = $this->jmsSerializer->serialize($notification, 'json');
		$data['message']  = $notificationJson;

		foreach ($notification->getFridgeMembersId() as $memberId) {
			$this->pusher->trigger('notifications', 'edit-product-fridge-' . $memberId, $data);
		}
	}

	public function deleteProductNotification(Notification $notification)
	{
		$notificationJson = $this->jmsSerializer->serialize($notification, 'json');
		$data['message']  = $notificationJson;

		foreach ($notification->getFridgeMembersId() as $memberId) {
			$this->pusher->trigger('notifications', 'delete-product-fridge-' . $memberId, $data);
		}
	}
}