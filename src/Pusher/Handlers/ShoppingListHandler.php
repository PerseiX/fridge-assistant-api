<?php

namespace App\Pusher\Handlers;

use App\Entity\ShoppingListItem;
use App\Representation\ShoppingListItemRepresentation;
use JMS\Serializer\Serializer;
use Pusher\Pusher;

/**
 * Class ShoppingListHandler
 * @package App\Pusher\Handlers
 */
class ShoppingListHandler
{
	/**
	 * @var Pusher
	 */
	private $pusher;

	/**
	 * @var Serializer
	 */
	private $jmsSerializer;

	/**
	 * FridgeHandler constructor.
	 *
	 * @param Pusher     $pusher
	 * @param Serializer $jmsSerializer
	 */
	public function __construct(Pusher $pusher, Serializer $jmsSerializer)
	{
		$this->pusher        = $pusher;
		$this->jmsSerializer = $jmsSerializer;
	}

	public function addShoppingListItem(ShoppingListItemRepresentation $representation)
	{
		$jsonInvitation  = $this->jmsSerializer->serialize($representation, 'json');
		$data['message'] = $jsonInvitation;
		$this->pusher->trigger('shoppingList', 'add-shopping-list-item-fridge-' . $representation->getFridge()->getId(), $data);
	}

	public function removeShoppingListItem(ShoppingListItemRepresentation $representation)
	{
		$jsonInvitation  = $this->jmsSerializer->serialize($representation, 'json');
		$data['message'] = $jsonInvitation;
		$this->pusher->trigger('shoppingList', 'remove-shopping-list-item-fridge-' . $representation->getFridge()->getId(), $data);
	}
}