<?php

namespace App\Representation;

use ApiBundle\Representation\RepresentationInterface;

/**
 * Class AbstractProductInstance
 * @package App\Representation
 */
class AbstractProductInstance implements RepresentationInterface
{
	/**
	 * @var int|null
	 */
	private $id;

	/**
	 * @var int|null
	 */
	private $amount;

	/**
	 * @var \DateTime
	 */
	private $createdAt;

	/**
	 * @var UserRepresentation|null
	 */
	private $createdBy;

	/**
	 * @var FridgeRepresentation|null
	 */
	private $fridge;

	/**
	 * @var ProductRepresentation|null
	 */
	private $product;

	/**
	 * @var string|null
	 */
	private $description;

	/**
	 * @return int|null
	 */
	public function getId(): ?int
	{
		return $this->id;
	}

	/**
	 * @param int|null $id
	 *
	 * @return AbstractProductInstance
	 */
	public function setId(?int $id): AbstractProductInstance
	{
		$this->id = $id;

		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getAmount(): ?int
	{
		return $this->amount;
	}

	/**
	 * @param int|null $amount
	 *
	 * @return AbstractProductInstance
	 */
	public function setAmount(?int $amount): AbstractProductInstance
	{
		$this->amount = $amount;

		return $this;
	}

	/**
	 * @return \DateTime
	 */
	public function getCreatedAt(): \DateTime
	{
		return $this->createdAt;
	}

	/**
	 * @param \DateTime $createdAt
	 *
	 * @return AbstractProductInstance
	 */
	public function setCreatedAt(\DateTime $createdAt): AbstractProductInstance
	{
		$this->createdAt = $createdAt;

		return $this;
	}

	/**
	 * @return UserRepresentation|null
	 */
	public function getCreatedBy(): ?UserRepresentation
	{
		return $this->createdBy;
	}

	/**
	 * @param UserRepresentation|null $createdBy
	 *
	 * @return AbstractProductInstance
	 */
	public function setCreatedBy(?UserRepresentation $createdBy): AbstractProductInstance
	{
		$this->createdBy = $createdBy;

		return $this;
	}

	/**
	 * @return FridgeRepresentation|null
	 */
	public function getFridge(): ?FridgeRepresentation
	{
		return $this->fridge;
	}

	/**
	 * @param FridgeRepresentation|null $fridge
	 *
	 * @return AbstractProductInstance
	 */
	public function setFridge(?FridgeRepresentation $fridge): AbstractProductInstance
	{
		$this->fridge = $fridge;

		return $this;
	}

	/**
	 * @return ProductRepresentation|null
	 */
	public function getProduct(): ?ProductRepresentation
	{
		return $this->product;
	}

	/**
	 * @param ProductRepresentation|null $product
	 *
	 * @return AbstractProductInstance
	 */
	public function setProduct(?ProductRepresentation $product): AbstractProductInstance
	{
		$this->product = $product;

		return $this;
	}

	/**
	 * @return null|string
	 */
	public function getDescription(): ?string
	{
		return $this->description;
	}

	/**
	 * @param null|string $description
	 *
	 * @return AbstractProductInstance
	 */
	public function setDescription(?string $description): AbstractProductInstance
	{
		$this->description = $description;

		return $this;
	}

}