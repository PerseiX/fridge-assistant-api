<?php

namespace App\Representation;

use ApiBundle\Representation\AbstractRepresentationCollection;

/**
 * Class FridgeCollectionRepresentation
 * @package App\Representation
 */
class FridgeCollectionRepresentation extends AbstractRepresentationCollection
{

}