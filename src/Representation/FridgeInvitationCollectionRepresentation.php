<?php

namespace App\Representation;

use ApiBundle\Representation\AbstractRepresentationCollection;

/**
 * Class FridgeInvitationCollectionRepresentation
 * @package App\Representation
 */
class FridgeInvitationCollectionRepresentation extends AbstractRepresentationCollection
{

}