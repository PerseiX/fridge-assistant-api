<?php

namespace App\Representation;

use ApiBundle\Representation\RepresentationInterface;

/**
 * Class FridgeInvitationRepresentation
 * @package App\Representation
 */
class FridgeInvitationRepresentation implements RepresentationInterface
{
	/**
	 * @var int|null
	 */
	private $id;

	/**
	 * @var UserRepresentation|null
	 */
	private $invitedUser;

	/**
	 * @var FridgeRepresentation|null
	 */
	private $invitedFridge;

	/**
	 * @var boolean|null
	 */
	private $accepted;

	/**
	 * @var \DateTime|null
	 */
	private $invitedAt;

	/**
	 * @return int|null
	 */
	public function getId(): ?int
	{
		return $this->id;
	}

	/**
	 * @param int|null $id
	 *
	 * @return FridgeInvitationRepresentation
	 */
	public function setId(?int $id): FridgeInvitationRepresentation
	{
		$this->id = $id;

		return $this;
	}

	/**
	 * @return UserRepresentation|null
	 */
	public function getInvitedUser(): ?UserRepresentation
	{
		return $this->invitedUser;
	}

	/**
	 * @param UserRepresentation|null $invitedUser
	 *
	 * @return FridgeInvitationRepresentation
	 */
	public function setInvitedUser(?UserRepresentation $invitedUser): FridgeInvitationRepresentation
	{
		$this->invitedUser = $invitedUser;

		return $this;
	}

	/**
	 * @return FridgeRepresentation|null
	 */
	public function getInvitedFridge(): ?FridgeRepresentation
	{
		return $this->invitedFridge;
	}

	/**
	 * @param FridgeRepresentation|null $invitedFridge
	 *
	 * @return FridgeInvitationRepresentation
	 */
	public function setInvitedFridge(?FridgeRepresentation $invitedFridge): FridgeInvitationRepresentation
	{
		$this->invitedFridge = $invitedFridge;

		return $this;
	}

	/**
	 * @return bool|null
	 */
	public function isAccepted(): ?bool
	{
		return $this->accepted;
	}

	/**
	 * @param bool|null $accepted
	 *
	 * @return FridgeInvitationRepresentation
	 */
	public function setAccepted(?bool $accepted): FridgeInvitationRepresentation
	{
		$this->accepted = $accepted;

		return $this;
	}

	/**
	 * @return \DateTime|null
	 */
	public function getInvitedAt(): ?\DateTime
	{
		return $this->invitedAt;
	}

	/**
	 * @param \DateTime|null $invitedAt
	 *
	 * @return FridgeInvitationRepresentation
	 */
	public function setInvitedAt(?\DateTime $invitedAt): FridgeInvitationRepresentation
	{
		$this->invitedAt = $invitedAt;

		return $this;
	}

}