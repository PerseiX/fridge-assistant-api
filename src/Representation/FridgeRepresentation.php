<?php

namespace App\Representation;

use ApiBundle\Representation\RepresentationInterface;

/**
 * Class FridgeRepresentation
 * @package App\Representation
 */
class FridgeRepresentation implements RepresentationInterface
{
	/**
	 * @var int|null
	 */
	private $id;

	/**
	 * @var string|null
	 */
	private $name;

	/**
	 * @var array[UserRepresentation]
	 */
	private $members;

	/**
	 * @var string|null
	 */
	private $slug;

	/**
	 * @var boolean|null
	 */
	private $active;

	/**
	 * @var \DateTime
	 */
	private $createdAt;

	/**
	 * @var UserRepresentation
	 */
	private $owner;

	/**
	 * @return int|null
	 */
	public function getId(): ?int
	{
		return $this->id;
	}

	/**
	 * @param int|null $id
	 *
	 * @return FridgeRepresentation
	 */
	public function setId(?int $id): FridgeRepresentation
	{
		$this->id = $id;

		return $this;
	}

	/**
	 * @return null|string
	 */
	public function getName(): ?string
	{
		return $this->name;
	}

	/**
	 * @param null|string $name
	 *
	 * @return FridgeRepresentation
	 */
	public function setName(?string $name): FridgeRepresentation
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * @return array
	 */
	public function getMembers(): ?array
	{
		return $this->members;
	}

	/**
	 * @param UserRepresentation $member
	 *
	 * @return FridgeRepresentation
	 */
	public function addMember(UserRepresentation $member): FridgeRepresentation
	{
		$this->members[] = $member;

		return $this;
	}

	/**
	 * @return null|string
	 */
	public function getSlug(): ?string
	{
		return $this->slug;
	}

	/**
	 * @param null|string $slug
	 *
	 * @return FridgeRepresentation
	 */
	public function setSlug(?string $slug): FridgeRepresentation
	{
		$this->slug = $slug;

		return $this;
	}

	/**
	 * @return bool|null
	 */
	public function isActive(): ?bool
	{
		return $this->active;
	}

	/**
	 * @param bool|null $active
	 *
	 * @return FridgeRepresentation
	 */
	public function setActive(?bool $active): FridgeRepresentation
	{
		$this->active = $active;

		return $this;
	}

	/**
	 * @return \DateTime
	 */
	public function getCreatedAt(): \DateTime
	{
		return $this->createdAt;
	}

	/**
	 * @param \DateTime $createdAt
	 *
	 * @return FridgeRepresentation
	 */
	public function setCreatedAt(\DateTime $createdAt): FridgeRepresentation
	{
		$this->createdAt = $createdAt;

		return $this;
	}

	/**
	 * @return UserRepresentation|null
	 */
	public function getOwner(): ?UserRepresentation
	{
		return $this->owner;
	}

	/**
	 * @param UserRepresentation|null $owner
	 *
	 * @return FridgeRepresentation
	 */
	public function setOwner(?UserRepresentation $owner): FridgeRepresentation
	{
		$this->owner = $owner;

		return $this;
	}

}