<?php

namespace App\Representation;

use ApiBundle\Representation\AbstractRepresentationCollection;

/**
 * Class NotificationCollectionRepresentation
 * @package App\Representation
 */
class NotificationCollectionRepresentation extends AbstractRepresentationCollection
{

}