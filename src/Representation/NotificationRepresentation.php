<?php

namespace App\Representation;

use ApiBundle\Representation\RepresentationInterface;

/**
 * Class Notification
 * @package App\Model
 */
class NotificationRepresentation implements RepresentationInterface
{
	/**
	 * @var string
	 */
	private $id;

	/**
	 * @var string
	 */
	private $key;

	/**
	 * @var string
	 */
	private $username;

	/**
	 * @var string
	 */
	private $avatar;

	/**
	 * @var string
	 */
	private $action;

	/**
	 * @var string
	 */
	private $fridgeSlug;

	/**
	 * @var string
	 */
	private $product;

	/**
	 * @var \DateTime
	 */
	private $createdAt;

	/**
	 * @var string
	 */
	private $context;

	/**
	 * @return string
	 */
	public function getId(): string
	{
		return $this->id;
	}

	/**
	 * @param string $id
	 *
	 * @return NotificationRepresentation
	 */
	public function setId(string $id): NotificationRepresentation
	{
		$this->id = $id;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getKey(): string
	{
		return $this->key;
	}

	/**
	 * @param string $key
	 *
	 * @return NotificationRepresentation
	 */
	public function setKey(string $key): NotificationRepresentation
	{
		$this->key = $key;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getUsername(): string
	{
		return $this->username;
	}

	/**
	 * @param string $username
	 *
	 * @return NotificationRepresentation
	 */
	public function setUsername(string $username): NotificationRepresentation
	{
		$this->username = $username;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getAvatar(): ?string
	{
		return $this->avatar;
	}

	/**
	 * @param string $avatar
	 *
	 * @return NotificationRepresentation
	 */
	public function setAvatar(?string $avatar): NotificationRepresentation
	{
		$this->avatar = $avatar;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getAction(): string
	{
		return $this->action;
	}

	/**
	 * @param string $action
	 *
	 * @return NotificationRepresentation
	 */
	public function setAction(string $action): NotificationRepresentation
	{
		$this->action = $action;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getFridgeSlug(): string
	{
		return $this->fridgeSlug;
	}

	/**
	 * @param string $fridgeSlug
	 *
	 * @return NotificationRepresentation
	 */
	public function setFridgeSlug(string $fridgeSlug): NotificationRepresentation
	{
		$this->fridgeSlug = $fridgeSlug;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getProduct(): string
	{
		return $this->product;
	}

	/**
	 * @param string $product
	 *
	 * @return NotificationRepresentation
	 */
	public function setProduct(string $product): NotificationRepresentation
	{
		$this->product = $product;

		return $this;
	}

	/**
	 * @return \DateTime
	 */
	public function getCreatedAt()
	{
		return $this->createdAt;
	}

	/**
	 * @param \DateTime $createdAt
	 *
	 * @return NotificationRepresentation
	 */
	public function setCreatedAt($createdAt): NotificationRepresentation
	{
		$this->createdAt = $createdAt;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getContext(): string
	{
		return $this->context;
	}

	/**
	 * @param string $context
	 *
	 * @return NotificationRepresentation
	 */
	public function setContext(string $context): NotificationRepresentation
	{
		$this->context = $context;

		return $this;
	}

}