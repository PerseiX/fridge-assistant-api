<?php

namespace App\Representation;

use ApiBundle\Representation\AbstractRepresentationCollection;

/**
 * Class ProductCollectionRepresentation
 * @package App\Representation
 */
class ProductCollectionRepresentation extends AbstractRepresentationCollection
{

}