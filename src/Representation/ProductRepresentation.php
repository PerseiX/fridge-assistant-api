<?php

namespace App\Representation;

use ApiBundle\Representation\RepresentationInterface;

/**
 * Class ProductRepresentation
 * @package App\Representation
 */
class ProductRepresentation implements RepresentationInterface
{
	/**
	 * @var int|null
	 */
	private $id;

	/**
	 * @var string|null
	 */
	private $name;

	/**
	 * @var string|null
	 */
	private $description;

	/**
	 * @return int|null
	 */
	public function getId(): ?int
	{
		return $this->id;
	}

	/**
	 * @param int|null $id
	 *
	 * @return ProductRepresentation
	 */
	public function setId(?int $id): ProductRepresentation
	{
		$this->id = $id;

		return $this;
	}

	/**
	 * @return null|string
	 */
	public function getName(): ?string
	{
		return $this->name;
	}

	/**
	 * @param null|string $name
	 *
	 * @return ProductRepresentation
	 */
	public function setName(?string $name): ProductRepresentation
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * @return null|string
	 */
	public function getDescription(): ?string
	{
		return $this->description;
	}

	/**
	 * @param null|string $description
	 *
	 * @return ProductRepresentation
	 */
	public function setDescription(?string $description): ProductRepresentation
	{
		$this->description = $description;

		return $this;
	}

}