<?php

namespace App\Representation;

use ApiBundle\Representation\AbstractRepresentationCollection;

/**
 * Class ShoppingListItemCollectionRepresentation
 * @package App\Representation
 */
class ShoppingListItemCollectionRepresentation extends AbstractRepresentationCollection
{

}