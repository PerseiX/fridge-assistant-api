<?php

namespace App\Representation;

use ApiBundle\Representation\RepresentationInterface;

/**
 * Class ShoppingListItem
 * @package App\Representation
 */
class ShoppingListItemRepresentation implements RepresentationInterface
{

	/**
	 * @var int|null
	 */
	private $id;

	/**
	 * @var int|null
	 */
	private $amount;

	/**
	 * @var \DateTime
	 */
	private $createdAt;

	/**
	 * @var UserRepresentation|null
	 */
	private $createdBy;

	/**
	 * @var FridgeRepresentation|null
	 */
	private $fridge;

	/**
	 * @var ProductRepresentation|null
	 */
	private $product;

	/**
	 * @var string|null
	 */
	private $description;

	/**
	 * @return int|null
	 */
	public function getId(): ?int
	{
		return $this->id;
	}

	/**
	 * @param int|null $id
	 *
	 * @return ShoppingListItemRepresentation
	 */
	public function setId(?int $id): ShoppingListItemRepresentation
	{
		$this->id = $id;

		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getAmount(): ?int
	{
		return $this->amount;
	}

	/**
	 * @param int|null $amount
	 *
	 * @return ShoppingListItemRepresentation
	 */
	public function setAmount(?int $amount): ShoppingListItemRepresentation
	{
		$this->amount = $amount;

		return $this;
	}

	/**
	 * @return \DateTime
	 */
	public function getCreatedAt(): \DateTime
	{
		return $this->createdAt;
	}

	/**
	 * @param \DateTime $createdAt
	 *
	 * @return ShoppingListItemRepresentation
	 */
	public function setCreatedAt(\DateTime $createdAt): ShoppingListItemRepresentation
	{
		$this->createdAt = $createdAt;

		return $this;
	}

	/**
	 * @return UserRepresentation|null
	 */
	public function getCreatedBy(): ?UserRepresentation
	{
		return $this->createdBy;
	}

	/**
	 * @param UserRepresentation|null $createdBy
	 *
	 * @return ShoppingListItemRepresentation
	 */
	public function setCreatedBy(?UserRepresentation $createdBy): ShoppingListItemRepresentation
	{
		$this->createdBy = $createdBy;

		return $this;
	}

	/**
	 * @return FridgeRepresentation|null
	 */
	public function getFridge(): ?FridgeRepresentation
	{
		return $this->fridge;
	}

	/**
	 * @param FridgeRepresentation|null $fridge
	 *
	 * @return ShoppingListItemRepresentation
	 */
	public function setFridge(?FridgeRepresentation $fridge): ShoppingListItemRepresentation
	{
		$this->fridge = $fridge;

		return $this;
	}

	/**
	 * @return ProductRepresentation|null
	 */
	public function getProduct(): ?ProductRepresentation
	{
		return $this->product;
	}

	/**
	 * @param ProductRepresentation|null $product
	 *
	 * @return ShoppingListItemRepresentation
	 */
	public function setProduct(?ProductRepresentation $product): ShoppingListItemRepresentation
	{
		$this->product = $product;

		return $this;
	}

	/**
	 * @return null|string
	 */
	public function getDescription(): ?string
	{
		return $this->description;
	}

	/**
	 * @param null|string $description
	 *
	 * @return ShoppingListItemRepresentation
	 */
	public function setDescription(?string $description): ShoppingListItemRepresentation
	{
		$this->description = $description;

		return $this;
	}

}