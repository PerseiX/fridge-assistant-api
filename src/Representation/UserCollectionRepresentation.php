<?php

namespace App\Representation;

use ApiBundle\Representation\AbstractRepresentationCollection;

/**
 * Class UserCollectionRepresentation
 * @package App\Representation
 */
class UserCollectionRepresentation extends AbstractRepresentationCollection
{

}