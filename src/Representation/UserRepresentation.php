<?php

namespace App\Representation;

use ApiBundle\Representation\RepresentationInterface;

/**
 * Class UserRepresentation
 * @package App\Representation
 */
class UserRepresentation implements RepresentationInterface
{

	/**
	 * @var integer
	 */
	protected $id;

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var string
	 */
	protected $surname;

	/**
	 * @var string
	 */
	protected $username;

	/**
	 * @var string
	 */
	protected $email;

	/**
	 * @var string
	 */
	protected $avatar;

	/**
	 * @var string
	 */
	protected $token;

	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 *
	 * @return UserRepresentation
	 */
	public function setId(int $id): UserRepresentation
	{
		$this->id = $id;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 *
	 * @return UserRepresentation
	 */
	public function setName(string $name): UserRepresentation
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getSurname(): string
	{
		return $this->surname;
	}

	/**
	 * @param string $surname
	 *
	 * @return UserRepresentation
	 */
	public function setSurname(string $surname): UserRepresentation
	{
		$this->surname = $surname;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getUsername(): string
	{
		return $this->username;
	}

	/**
	 * @param string $username
	 *
	 * @return UserRepresentation
	 */
	public function setUsername(string $username): UserRepresentation
	{
		$this->username = $username;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getEmail(): string
	{
		return $this->email;
	}

	/**
	 * @param string $email
	 *
	 * @return UserRepresentation
	 */
	public function setEmail(string $email): UserRepresentation
	{
		$this->email = $email;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getAvatar(): ?string
	{
		return $this->avatar;
	}

	/**
	 * @param string $avatar
	 *
	 * @return UserRepresentation
	 */
	public function setAvatar(string $avatar = null): UserRepresentation
	{
		$this->avatar = $avatar;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getToken()
	{
		return $this->token;
	}

	/**
	 * @param string $token
	 *
	 * @return UserRepresentation
	 */
	public function setToken(string $token = null): UserRepresentation
	{
		$this->token = $token;

		return $this;
	}

}