<?php

namespace App\Resolver;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class ImagePathResolver
 * @package App\Resolver
 */
class ImagePathResolver
{
	/**
	 * @var Request
	 */
	private $request;

	/**
	 * ImagePathResolver constructor.
	 *
	 * @param RequestStack $request
	 */
	public function __construct(RequestStack $request)
	{
		$this->request = $request->getMasterRequest();
	}

	public function resolve(string $fileName = null, string $fileDirectory = '')
	{
		if (!$fileName) {
			return null;
		}

		$filePath = sprintf('%s/uploads%s/%s', $this->request->getSchemeAndHttpHost(), $fileDirectory, $fileName);

		return $filePath;
	}
}