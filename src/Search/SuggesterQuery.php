<?php

namespace App\Search;

use App\Entity\User;
use App\Model\QueryModel;
use Doctrine\ORM\EntityManager;
use Elastica\Query;
use Elastica\Query\BoolQuery;
use Elastica\Query\MatchPhrasePrefix;
use FOS\ElasticaBundle\Finder\PaginatedFinderInterface;

/**
 * Class SuggesterQuer
 * @package App\Search
 */
class SuggesterQuery
{
	/**
	 * PaginatedFinderInterface
	 */
	private $index;

	/**
	 * @var EntityManager
	 */
	private $em;

	/**
	 * SuggesterQuery constructor.
	 *
	 * @param               $index
	 * @param EntityManager $em
	 */
	public function __construct(PaginatedFinderInterface $index, EntityManager $em)
	{
		$this->index = $index;
		$this->em    = $em;
	}

	/**
	 * @param QueryModel $queryModel
	 *
	 * @return mixed
	 */
	public function getSuggestedEmails(QueryModel $queryModel)
	{
		$query = new BoolQuery();

		$this->attacheEmailPhrase($query, $queryModel);
		$this->excludeOwner($query, $queryModel);
		$this->excludeExistingMembers($query, $queryModel);

		return $this->index->find($query);
	}

	/**
	 * @param BoolQuery  $query
	 * @param QueryModel $queryModel
	 */
	private function attacheEmailPhrase(BoolQuery $query, QueryModel $queryModel)
	{
		$prefixQuery = new  MatchPhrasePrefix();
		$prefixQuery->setField('email', $queryModel->getPhrase());
		$query->addMust($prefixQuery);
	}

	/**
	 * @param BoolQuery  $query
	 * @param QueryModel $queryModel
	 */
	private function excludeOwner(BoolQuery $query, QueryModel $queryModel)
	{
		$email         = $queryModel->getUser()->getEmail();
		$notOwnerQuery = new MatchPhrasePrefix();
		$notOwnerQuery->setField('email', $email);

		$query->addMustNot($notOwnerQuery);
	}

	/**
	 * @param BoolQuery  $query
	 * @param QueryModel $queryModel
	 */
	private function excludeExistingMembers(BoolQuery $query, QueryModel $queryModel)
	{
		$notOwnerQuery = new MatchPhrasePrefix();
		$notOwnerQuery->setField('fridges.id', $queryModel->getFridgeId());

		$query->addMustNot($notOwnerQuery);
	}
}