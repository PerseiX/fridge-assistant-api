<?php

namespace App\Security\Voters;

use ApiBundle\Security\Voter\AbstractVoter;
use App\Entity\Fridge;
use App\Entity\User;
use FOS\ElasticaBundle\Logger\ElasticaLogger;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * Class FridgeVoter
 * @package App\Security\Voters
 */
class FridgeVoter extends AbstractVoter
{
	const LEAVE_FRIDGE  = 'PERMISSION_LEAVE_FRIDGE';
	const REMOVE_MEMBER = 'PERMISSION_REMOVE_MEMBER';

	/**
	 * @return array
	 */
	protected function getSupportedClass(): array
	{
		return [
			Fridge::class,
			User::class
		];
	}

	/**
	 * @return array
	 */
	protected function getSupportedPermissions(): array
	{
		return [
			self::DELETE,
			self::LEAVE_FRIDGE,
			self::REMOVE_MEMBER
		];
	}

	/**
	 * @param string $attribute
	 * @param mixed  $subject
	 *
	 * @return bool
	 */
	protected function supports($attribute, $subject): bool
	{
		//TODO hmmm? Something more clear?
		if (is_array($subject)) {
			foreach ($subject as $item) {
				if (!in_array(get_class($item), $this->getSupportedClass())) {
					return false;
				}
			}
		} else {
			if (!in_array(get_class($subject), $this->getSupportedClass())) {
				return false;
			}
		}

		return in_array($attribute, $this->getSupportedPermissions());
	}

	/**
	 * @param string         $attribute
	 * @param mixed          $subject
	 * @param TokenInterface $token
	 *
	 * @return bool
	 */
	protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
	{
		/** @var User $user */
		$user = $token->getUser();

		switch ($attribute) {
			case self::LEAVE_FRIDGE:
				{
					return $this->canLeaveFridge($subject, $user);
				}
			case self::DELETE:
				{
					return $this->canDelete($subject, $user);
				}
			case self::REMOVE_MEMBER:
				{
					$userToRemove = $subject[1];
					$fridge       = $subject[0];

					return $this->canRemoveMember($user, $fridge, $userToRemove);
				}
		}

		return false;
	}

	/**
	 * @param Fridge        $fridge
	 * @param UserInterface $user
	 *
	 * @return bool
	 */
	private function canLeaveFridge(Fridge $fridge, UserInterface $user)
	{
		return $fridge->getMembers()->contains($user);
	}

	/**
	 * @param Fridge        $fridge
	 * @param UserInterface $user
	 *
	 * @return bool
	 */
	private function canDelete(Fridge $fridge, UserInterface $user)
	{
		return $user->getId() === $fridge->getOwner()->getId();
	}

	/**
	 * @param User   $owner
	 * @param Fridge $fridge
	 * @param User   $user
	 *
	 * @return bool
	 */
	private function canRemoveMember(User $owner, Fridge $fridge, User $user)
	{
		if ($fridge->getOwner()->getId() !== $owner->getId()) {
			return false;
		}

		if (false === $fridge->getMembers()->contains($user)) {
			return false;
		}

		return true;
	}
}