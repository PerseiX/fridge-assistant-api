<?php

namespace App\Security\Voters;

use ApiBundle\Security\Voter\AbstractVoter;
use App\Entity\FridgeInvitation;
use App\Entity\User;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * Class InvitationVoter
 * @package App\Security\Voters
 */
class InvitationVoter extends AbstractVoter
{

	const ACCEPT_INVITATION = 'PERMISSION_ACCEPT_INVITATION';
	const REJECT_INVITATION = 'PERMISSION_REJECT_INVITATION';

	/**
	 * @return array
	 */
	protected function getSupportedClass(): array
	{
		return [FridgeInvitation::class];
	}

	/**
	 * @return array
	 */
	protected function getSupportedPermissions(): array
	{
		return [
			self::ACCEPT_INVITATION,
			self::REJECT_INVITATION
		];
	}

	/**
	 * @param string         $attribute
	 * @param mixed          $subject
	 * @param TokenInterface $token
	 *
	 * @return bool
	 */
	protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
	{
		/** @var User $user */
		$user = $token->getUser();

		switch ($attribute) {
			case self::REJECT_INVITATION:
				{
					return $this->canRejectInvitation($subject, $user);
				}
			case self::ACCEPT_INVITATION:
				{
					return $this->canAcceptInvitation($subject, $user);
				}
		}
	}

	/**
	 * @param FridgeInvitation $invitation
	 * @param UserInterface    $user
	 *
	 * @return bool
	 */
	private function canRejectInvitation(FridgeInvitation $invitation, UserInterface $user)
	{
		return $invitation->getInvitedUser()->getId() === $user->getId();
	}

	/**
	 * @param FridgeInvitation $invitation
	 * @param UserInterface    $user
	 *
	 * @return bool
	 */
	private function canAcceptInvitation(FridgeInvitation $invitation, UserInterface $user)
	{
		return $invitation->getInvitedUser()->getId() === $user->getId();
	}
}