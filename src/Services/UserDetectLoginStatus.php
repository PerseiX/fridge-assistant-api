<?php

namespace App\Services;

use Lexik\Bundle\JWTAuthenticationBundle\Encoder\LcobucciJWTEncoder;
use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ORM\EntityManager;

/**
 * Class UserDetectLoginStatus
 * @package App\Services
 */
class UserDetectLoginStatus
{
	/**
	 * @var RequestStack
	 */
	private $request;

	/**
	 * @var EntityManager
	 */
	private $em;

	/**
	 * @var LcobucciJWTEncoder
	 */
	private $encoder;

	/**
	 * UserDetectLoginStatus constructor.
	 *
	 * @param RequestStack       $requestStack
	 * @param EntityManager      $em
	 * @param LcobucciJWTEncoder $encoder
	 */
	public function __construct(RequestStack $requestStack, EntityManager $em, LcobucciJWTEncoder $encoder)
	{
		$this->request = $requestStack->getMasterRequest();
		$this->em      = $em;
		$this->encoder = $encoder;
	}

	/**
	 * @return bool
	 * @throws \Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException
	 */
	public function theSameEmail()
	{
		$email = $this->request->get('email');

		$tokenBearer = $this->request->headers->get('Authorization');
		if (null !== $tokenBearer) {
			$token = substr($tokenBearer, 7);
			if ('null' !== $token) {
				$decodedUser = $this->encoder->decode($token);
				$userNew     = $this->em->getRepository('App:User')->find($decodedUser['id']);
				if ($userNew->getEmail() === $email) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * @return bool
	 * @throws \Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException
	 */
	public function theSameUsername()
	{
		$username = $this->request->get('username');

		$tokenBearer = $this->request->headers->get('Authorization');
		if (null !== $tokenBearer) {
			$token = substr($tokenBearer, 7);
			if ('null' !== $token) {
				$decodedUser = $this->encoder->decode($token);
				$userNew     = $this->em->getRepository('App:User')->find($decodedUser['id']);
				if ($userNew->getUsername() === $username) {
					return true;
				}
			}
		}

		return false;
	}
}