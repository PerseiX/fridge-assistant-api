<?php

namespace App\Transformer;

use App\Entity\RequestUser;
use App\Entity\User;

/**
 * Class RequestUserTransformer
 * @package App\Transformer
 */
class RequestUserTransformer
{
	/**
	 * @param RequestUser $requestUser
	 *
	 * @return User
	 */
	public function transform(RequestUser $requestUser): User
	{
		$user = new User();
		$user->setUsername($requestUser->getUsername())
		     ->setName($requestUser->getName())
		     ->setSurname($requestUser->getSurname())
		     ->setEmail($requestUser->getEmail())
		     ->setPlainPassword($requestUser->getPassword())
		     ->setEnabled((bool)true)
		     ->setSuperAdmin((bool)false);

		return $user;
	}
}