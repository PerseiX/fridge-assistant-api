<?php

namespace App\Transformers\Collection;

use App\Representation\FridgeInvitationCollectionRepresentation;
use ApiBundle\Representation\RepresentationInterface;
use App\Model\FridgeInvitationCollectionModel;
use ApiBundle\Transformer\AbstractTransformer;
use ApiBundle\Model\AbstractModelCollection;

/**
 * Class FridgeCollectionTransformer
 * @package App\Transformers\Collection
 */
class FridgeInvitationCollectionTransformer extends AbstractTransformer
{
	/**
	 * @param $input
	 *
	 * @return bool
	 */
	public function support($input): bool
	{
		return $input instanceof FridgeInvitationCollectionModel;
	}

	/**
	 * @param $input
	 *
	 * @return RepresentationInterface
	 */
	public function transform($input): RepresentationInterface
	{
		$collection = [];
		/** @var AbstractModelCollection $input */
		foreach ($input->getCollection() as $item) {
			$collection[] = $this->getTransformer()->transform($item);
		}

		return new FridgeInvitationCollectionRepresentation($collection);
	}
}