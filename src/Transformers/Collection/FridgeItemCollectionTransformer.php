<?php

namespace App\Transformers\Collection;

use ApiBundle\Model\AbstractModelCollection;
use ApiBundle\Representation\RepresentationInterface;
use ApiBundle\Transformer\AbstractTransformer;
use App\Model\FridgeItemCollectionModel;
use App\Model\ShoppingListItemCollectionModel;
use App\Representation\FridgeItemCollectionRepresentation;
use App\Representation\ShoppingListItemCollectionRepresentation;

/**
 * Class ShoppingListCollectionTransformer
 * @package App\Transformers\Collection
 */
class FridgeItemCollectionTransformer extends AbstractTransformer
{
	/**
	 * @param $input
	 *
	 * @return bool
	 */
	public function support($input): bool
	{
		return $input instanceof FridgeItemCollectionModel;
	}

	/**
	 * @param $input
	 *
	 * @return RepresentationInterface
	 */
	public function transform($input): RepresentationInterface
	{
		$collection = [];
		/** @var AbstractModelCollection $input */
		foreach ($input->getCollection() as $item) {
			$collection[] = $this->getTransformer()->transform($item);
		}

		return new FridgeItemCollectionRepresentation($collection);
	}
}