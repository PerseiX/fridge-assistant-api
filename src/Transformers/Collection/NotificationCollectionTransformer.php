<?php

namespace App\Transformers\Collection;

use ApiBundle\Model\AbstractModelCollection;
use ApiBundle\Representation\RepresentationInterface;
use ApiBundle\Transformer\AbstractTransformer;
use App\Model\FridgeCollectionModel;
use App\Model\NotificationCollectionModel;
use App\Representation\FridgeCollectionRepresentation;
use App\Representation\NotificationCollectionRepresentation;

/**
 * Class NotificationCollectionTransformer
 * @package App\Transformers\Collection
 */
class NotificationCollectionTransformer extends AbstractTransformer
{
	/**
	 * @param $input
	 *
	 * @return bool
	 */
	public function support($input): bool
	{
		return $input instanceof NotificationCollectionModel;
	}

	/**
	 * @param $input
	 *
	 * @return RepresentationInterface
	 */
	public function transform($input): RepresentationInterface
	{
		$collection = [];
		/** @var AbstractModelCollection $input */
		foreach ($input->getCollection() as $item) {
			$collection[] = $this->getTransformer()->transform($item);
		}

		return new NotificationCollectionRepresentation($collection);
	}
}