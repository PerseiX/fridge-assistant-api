<?php

namespace App\Transformers\Collection;

use ApiBundle\Model\AbstractModelCollection;
use ApiBundle\Representation\RepresentationInterface;
use ApiBundle\Transformer\AbstractTransformer;
use App\Model\FridgeCollectionModel;
use App\Model\ProductCollectionModel;
use App\Representation\FridgeCollectionRepresentation;
use App\Representation\ProductCollectionRepresentation;

/**
 * Class ProductCollectionTransformer
 * @package App\Transformers\Collection
 */
class ProductCollectionTransformer extends AbstractTransformer
{
	/**
	 * @param $input
	 *
	 * @return bool
	 */
	public function support($input): bool
	{
		return $input instanceof ProductCollectionModel;
	}

	/**
	 * @param $input
	 *
	 * @return RepresentationInterface
	 */
	public function transform($input): RepresentationInterface
	{
		$collection = [];
		/** @var AbstractModelCollection $input */
		foreach ($input->getCollection() as $item) {
			$collection[] = $this->getTransformer()->transform($item);
		}

		return new ProductCollectionRepresentation($collection);
	}
}