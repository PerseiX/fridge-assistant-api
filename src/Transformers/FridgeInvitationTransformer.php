<?php

namespace App\Transformers;

use ApiBundle\Representation\RepresentationInterface;
use ApiBundle\Transformer\AbstractTransformer;
use App\Entity\FridgeInvitation;
use App\Representation\FridgeInvitationRepresentation;

/**
 * Class FridgeInvitationTransformer
 * @package App\Transformers
 */
class FridgeInvitationTransformer extends AbstractTransformer
{

	/**
	 * @param $input
	 *
	 * @return bool
	 */
	public function support($input): bool
	{
		return $input instanceof FridgeInvitation;
	}

	/**
	 * @param $input
	 *
	 * @return RepresentationInterface
	 */
	public function transform($input): RepresentationInterface
	{
		$fridgeInvitationRepresentation = new FridgeInvitationRepresentation();

		/** @var FridgeInvitation $input */
		if ($input instanceof FridgeInvitation) {
			$fridgeInvitationRepresentation
				->setId($input->getId())
				->setInvitedUser($this->transformer->transform($input->getInvitedUser()))
				->setInvitedFridge($this->transformer->transform($input->getInvitedFridge()))
				->setInvitedAt($input->getInvitedAt())
				->setAccepted($input->getAccepted());
		}

		return $fridgeInvitationRepresentation;
	}
}