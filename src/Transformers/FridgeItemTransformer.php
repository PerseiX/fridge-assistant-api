<?php

namespace App\Transformers;

use ApiBundle\Representation\RepresentationInterface;
use App\Entity\FridgeItem;
use App\Representation\FridgeItemRepresentation;
use ApiBundle\Transformer\AbstractTransformer;
use App\Entity\ShoppingListItem;
use App\Entity\Fridge;
use App\Representation\FridgeRepresentation;
use App\Representation\ProductRepresentation;
use App\Representation\UserRepresentation;

/**
 * Class FridgeItemTransformer
 * @package App\Transformers
 */
class FridgeItemTransformer extends AbstractTransformer
{

	/**
	 * @param $input
	 *
	 * @return bool
	 */
	public function support($input): bool
	{
		return $input instanceof FridgeItem;
	}

	/**
	 * @param $input
	 *
	 * @return RepresentationInterface
	 */
	public function transform($input): RepresentationInterface
	{
		$representation = new FridgeItemRepresentation();

		/** @var ShoppingListItem $input */
		if (true === $input instanceof FridgeItem) {
			/** @var ProductRepresentation $productRepresentation */
			$productRepresentation = $this->transformer->transform($input->getProduct());
			/** @var UserRepresentation $authorRepresentation */

			$authorRepresentation = $this->transformer->transform($input->getCreatedBy());
			$representation
				->setId($input->getId())
				->setCreatedAt($input->getCreatedAt())
				->setAmount($input->getAmount())
				->setDescription($input->getDescription())
				->setProduct($productRepresentation)
				->setCreatedBy($authorRepresentation);

			if (true === $input->getFridge() instanceof Fridge) {
				/** @var FridgeRepresentation $fridgeRepresentation */
				$fridgeRepresentation = $this->transformer->transform($input->getFridge());
				$representation->setFridge($fridgeRepresentation);
			}
		}

		return $representation;
	}
}