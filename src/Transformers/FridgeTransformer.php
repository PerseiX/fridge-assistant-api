<?php

namespace App\Transformers;

use ApiBundle\Representation\RepresentationInterface;
use ApiBundle\Transformer\AbstractTransformer;
use App\Entity\Fridge;
use App\Representation\FridgeRepresentation;

/**
 * Class FridgeTransformer
 * @package App\Transformers
 */
class FridgeTransformer extends AbstractTransformer
{

	/**
	 * @param $input
	 *
	 * @return bool
	 */
	public function support($input): bool
	{
		return $input instanceof Fridge;
	}

	/**
	 * @param $input
	 *
	 * @return RepresentationInterface
	 */
	public function transform($input): RepresentationInterface
	{
		$representation = new FridgeRepresentation();

		/** @var Fridge $input */
		if (true === $input instanceof Fridge) {
			$representation
				->setId($input->getId())
				->setName($input->getName())
				->setActive($input->isActive())
				->setSlug($input->getSlug())
				->setCreatedAt($input->getCreatedAt())
				->setOwner($this->transformer->transform($input->getOwner()));

			foreach ($input->getMembers() as $member) {
				$representation->addMember($this->transformer->transform($member));
			}
		}

		return $representation;
	}
}