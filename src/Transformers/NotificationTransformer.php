<?php

namespace App\Transformers;

use ApiBundle\Representation\RepresentationInterface;
use App\Representation\NotificationRepresentation;
use ApiBundle\Transformer\AbstractTransformer;
use App\Model\Notification;

/**
 * Class NotificationTransformer
 * @package App\Transformers
 */
class NotificationTransformer extends AbstractTransformer
{

	/**
	 * @param $input
	 *
	 * @return bool
	 */
	public function support($input): bool
	{
		return $input instanceof Notification;
	}

	/**
	 * @param $input
	 *
	 * @return RepresentationInterface
	 */
	public function transform($input): RepresentationInterface
	{
		$representation = new NotificationRepresentation();

		if (true === $input instanceof Notification) {
			/** @var Notification $input */
			$representation
				->setId($input->getKey())
				->setProduct($input->getProduct())
				->setUsername($input->getUsername())
				->setFridgeSlug($input->getFridgeSlug())
				->setAvatar($input->getAvatar())
				->setKey($input->getKey())
				->setAction($input->getAction())
				->setContext($input->getContext());
		}

		return $representation;
	}
}