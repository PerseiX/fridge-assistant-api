<?php

namespace App\Transformers;

use ApiBundle\Representation\RepresentationInterface;
use ApiBundle\Transformer\AbstractTransformer;
use App\Entity\Product;
use App\Representation\ProductRepresentation;

/**
 * Class ProductTransformer
 * @package App\Transformers
 */
class ProductTransformer extends AbstractTransformer
{

	/**
	 * @param $input
	 *
	 * @return bool
	 */
	public function support($input): bool
	{
		return $input instanceof Product;
	}

	/**
	 * @param $input
	 *
	 * @return RepresentationInterface
	 */
	public function transform($input): RepresentationInterface
	{
		$representation = new ProductRepresentation();

		/** @var Product $input */
		if (true === $input instanceof Product) {
			$representation
				->setId($input->getId())
				->setName($input->getName())
				->setDescription($input->getDescription());
		}

		return $representation;
	}
}