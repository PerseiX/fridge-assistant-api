<?php

namespace App\Transformers;

use ApiBundle\Representation\RepresentationInterface;
use ApiBundle\Transformer\AbstractTransformer;
use App\Entity\Fridge;
use App\Entity\ShoppingListItem;
use App\Representation\ShoppingListItemRepresentation;

/**
 * Class ShoppingListItemTransformer
 * @package App\Transformers
 */
class ShoppingListItemTransformer extends AbstractTransformer
{

	/**
	 * @param $input
	 *
	 * @return bool
	 */
	public function support($input): bool
	{
		return $input instanceof ShoppingListItem;
	}

	/**
	 * @param $input
	 *
	 * @return RepresentationInterface
	 */
	public function transform($input): RepresentationInterface
	{
		$representation = new ShoppingListItemRepresentation();

		/** @var ShoppingListItem $input */
		if (true === $input instanceof ShoppingListItem) {
			$representation
				->setId($input->getId())
				->setCreatedAt($input->getCreatedAt())
				->setAmount($input->getAmount())
				->setDescription($input->getDescription())
				->setProduct($this->transformer->transform($input->getProduct()))
				->setCreatedBy($this->transformer->transform($input->getCreatedBy()));

			if ($input->getFridge() instanceof Fridge) {
				$representation->setFridge($this->transformer->transform($input->getFridge()));
			}
		}

		return $representation;
	}
}