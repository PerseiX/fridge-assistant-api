<?php

namespace App\Transformers;

use ApiBundle\Representation\RepresentationInterface;
use ApiBundle\Transformer\AbstractTransformer;
use App\Entity\User;
use App\Representation\UserRepresentation;
use App\Resolver\ImagePathResolver;

/**
 * Class UserTransformer
 * @package App\Transformers
 */
class UserTransformer extends AbstractTransformer
{
	/**
	 * @var ImagePathResolver
	 */
	private $imagePathResolver;

	/**
	 * @param ImagePathResolver $imagePathResolver
	 */
	public function setImagePathResolver(ImagePathResolver $imagePathResolver)
	{
		$this->imagePathResolver = $imagePathResolver;
	}

	/**
	 * @param $input
	 *
	 * @return bool
	 */
	public function support($input): bool
	{
		return $input instanceof User;
	}

	/**
	 * @param $input
	 *
	 * @return RepresentationInterface
	 */
	public function transform($input): RepresentationInterface
	{
		$representation = new UserRepresentation();
		/** @var User $input */
		if (true === $input instanceof User) {
			$representation
				->setId($input->getId())
				->setSurname($input->getSurname())
				->setName($input->getName())
				->setUsername($input->getUsername())
				->setAvatar($this->imagePathResolver->resolve($input->getAvatar(), '/avatar'))
				->setEmail($input->getEmail());
		}

		return $representation;
	}
}