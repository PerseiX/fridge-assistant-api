<?php

namespace App\Utility;

/**
 * Class KeyGenerator
 * @package App\Utility
 */
class KeyGenerator
{
	/**
	 * @param int    $userId
	 * @param string $type
	 *
	 * @return string
	 */
	public static function generateKey(int $userId, string $type = null)
	{
		if (null === $type) {
			return sprintf('%s_*', $userId);
		}

		return sprintf('%s_%s_*', $userId, $type);
	}
}