<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class UniqueField extends Constraint
{
	public $field = null;
	public $class = null;
	public $message = 'The field "{{ string }}" is already used';

	public function getTargets()
	{
		return self::CLASS_CONSTRAINT;
	}

	public function getRequiredOptions()
	{
		return [
			'field',
			'class'
		];
	}

	public function validatedBy()
	{
		return \get_class($this).'Validator';
	}
}