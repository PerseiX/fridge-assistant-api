<?php

namespace App\Validator\Constraints;

use App\Entity\RequestUser;
use Doctrine\ORM\EntityManager;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class UniqueFiledValidator
 * @package App\Validator\Constraints
 */
class UniqueFieldValidator extends ConstraintValidator
{

	/**
	 * @var EntityManager
	 */
	private $em;

	/**
	 * UniqueFiledValidator constructor.
	 *
	 * @param EntityManager $em
	 */
	public function __construct(EntityManager $em)
	{
		$this->em = $em;
	}

	public function validate($value, Constraint $constraint)
	{
		if (null === $value || false === $value instanceof RequestUser) {
			return;
		}
		/** @var RequestUser $value */
		$accessor    = PropertyAccess::createPropertyAccessor();
		$data        = $accessor->getValue($value, $constraint->field);
		$existedUser = $this->em->getRepository($constraint->class)->findBy([$constraint->field => $data]);

		if (false ===  empty($existedUser)) {
			$this->context->buildViolation($constraint->message)
			              ->setParameter('{{ string }}', $constraint->field)
			              ->addViolation();
		}
	}

	public function validatedBy()
	{
		return \get_class($this) . 'Validator';
	}
}